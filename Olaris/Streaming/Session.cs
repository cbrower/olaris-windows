﻿using Flurl;
using Flurl.Http;
using Olaris.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Windows.Media.Core;

namespace Olaris.Streaming
{
    /// <summary>
    /// A streaming session for a particular media item.
    /// </summary>
    public class Session
    {
        private OlarisServer _server;
        private CreateSTResponse _stResponse;

        /// <summary>
        /// Constructor
        /// </summary>
        public Session(OlarisServer server, CreateSTResponse stResponse)
        {
            _server = server ?? throw new ArgumentNullException(nameof(server));
            _stResponse = stResponse ?? throw new ArgumentNullException(nameof(stResponse));
        }

        /// <summary>
        /// Returns the DASH streaming URL for the session.
        /// </summary>
        public async Task<Url> GetDashUrl()
        {
            var codecs = await GetPlayableCodecs();
            return _server.BaseUrl
                .AppendPathSegment(_stResponse.DashStreamingPath)
                .SetQueryParam("playableCodecs", codecs);
        }

        /// <summary>
        /// Returns the HLS streaming URL for the session.
        /// </summary>
        public async Task<Url> GetHlsUrl()
        {
            var codecs = await GetPlayableCodecs();
            return _server.BaseUrl
                .AppendPathSegment(_stResponse.HlsStreamingPath)
                .SetQueryParam("playableCodecs", codecs);
        }

        /// <summary>
        /// Fetches the metadata info for the streaming session.
        /// </summary>
        /// <returns></returns>
        public async Task<Metadata> FetchMetadata()
        {
            var metadata = await _server.BaseUrl
                .AppendPathSegment(_stResponse.MetadataPath)
                .GetJsonAsync<Metadata>();
            
            return metadata;
        }

        /// <summary>
        /// Fetches the metadata and returns the supported codecs out of the
        /// codec list.
        /// </summary>
        /// <returns></returns>
        private async Task<IList<string>> GetPlayableCodecs()
        {
            var metadata = await FetchMetadata();
            var playableCodecs = new List<string>();

            foreach (var codec in metadata.CheckCodecs)
            {
                var normalized = NormalizeCodec(codec);
                if (await CodecIsPlayable(normalized))
                {
                    playableCodecs.Add(codec);
                }
            }

            return playableCodecs;
        }

        /// <summary>
        /// Returns true if the system can play the provided codec.
        /// </summary>
        /// <param name="codecString">The codec string to evaluate</param>
        /// <returns>True if playable</returns>
        private async Task<bool> CodecIsPlayable(string codecString)
        {
            if (codecString == null)
            {
                return false;
            }
            
            // Try querying for a video codec
            try
            {
                var videoQuery = new CodecQuery();
                IReadOnlyList<CodecInfo> videoResults = await videoQuery.FindAllAsync(CodecKind.Video, CodecCategory.Decoder, codecString);

                if (videoResults.Count > 0)
                {
                    return true;
                }
            }
            catch (Exception _)
            {
                // Do nothing - fall through to audio query
            }

            // Try querying for an audio codec
            try
            {
                var audioQuery = new CodecQuery();
                IReadOnlyList<CodecInfo> audioResults = await audioQuery.FindAllAsync(CodecKind.Audio, CodecCategory.Decoder, codecString);

                if (audioResults.Count > 0)
                {
                    return true;
                }
            }
            catch (Exception _)
            {
                // Do nothing - continue on and return false
            }

            return false;
        }

        /// <summary>
        /// Normalizes the codec strings coming from Olaris so they can be used
        /// to query the system's codecs.
        /// </summary>
        /// <param name="codecString"></param>
        /// <returns></returns>
        private string NormalizeCodec(string codecString)
        {
            var baseCodec = codecString.Split(".").FirstOrDefault();
            
            if (string.IsNullOrEmpty(baseCodec))
            {
                return null;
            }
            
            switch (baseCodec.ToUpper())
            {
                case "AVC1":
                    return "H264";
                case "VC1":
                    return "WVC1";
                default:
                    return baseCodec.ToUpper();
            }
        }
    }
}
