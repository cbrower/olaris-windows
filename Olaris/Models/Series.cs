﻿using Olaris.Data;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.UI.Xaml.Data;

namespace Olaris.Models
{
    public class Series : ISearchItem, ICardItem
    {
        public string Name { get; set; }
        public string OriginalName { get; set; }
        public string Overview { get; set; }
        public string FirstAirDate { get; set; }
        public string Status { get; set; }
        public List<Season> Seasons { get; set; }
        public string BackdropPath { get; set; }
        public string PosterPath { get; set; }
        public int TmdbId { get; set; }
        public string Type { get; set; }
        public string Uuid { get; set; }
        public int UnwatchedEpisodesCount { get; set; }

        /// <summary>
        /// Returns true if the series has an original name that is different
        /// than it's English name.
        /// </summary>
        public bool HasDifferentOriginalName
        {
            get
            {
                return Name != OriginalName;
            }
        }

        /// <summary>
        /// Converts the image paths for this Series instance to absolute URLs based on a particular Olaris server.
        /// </summary>
        /// <param name="server"></param>
        public void ConvertToAbsoluteImagePaths(OlarisServer server)
        {
            if (!string.IsNullOrEmpty(PosterPath)) PosterPath = server.GetFullImagePath(PosterPath, PosterSize.w500);
            if (!string.IsNullOrEmpty(BackdropPath)) BackdropPath = server.GetFullImagePath(BackdropPath, BackdropSize.original);

            // Convert the URLs for any seasons in this series.
            if (Seasons != null)
            {
                foreach (Season item in Seasons)
                {
                    item.ConvertToAbsoluteImagePaths(server);
                }
            }
        }

        #region Implementation: ICardItem

        public int CardCount
        {
            get => UnwatchedEpisodesCount;
        }

        public int CardMinCount
        {
            get => 1;
        }
        public string CardPosterPath
        {
            get => PosterPath;
        }
        public string CardTitle
        {
            get => Name;
        }
        public string CardSubtitle
        {
            get => string.Format("{0} Seasons", Seasons.Count);
        }
        public bool CardWatched
        {
            get => true;
        }
        public double CardProgress
        {
            get => 0;
        }

        #endregion
    }

    /// <summary>
    /// Series collection that can be used as a data source for UWP's infinite scrolling list types.
    /// </summary>
    public class IncrementalLoadingSeriesCollection : ObservableCollection<Series>, ISupportIncrementalLoading
    {
        private int _lastBatchSize = -1;
        private int _lastIndex = 0;
        private IDataSource _dataSource;
        private SeriesSortOption _seriesSortOption;

        public bool HasMoreItems
        {
            get => _lastBatchSize > 0;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="dataSource"></param>
        public IncrementalLoadingSeriesCollection(IDataSource dataSource, SeriesSortOption seriesSortOption)
        {
            _dataSource = dataSource;
            _seriesSortOption = seriesSortOption;
        }

        // the count is the number requested
        public IAsyncOperation<LoadMoreItemsResult> LoadMoreItemsAsync(uint count)
        {
            return AsyncInfo.Run(async cancelToken =>
            {
                var series = await _dataSource.GetSeries(_lastIndex, 25, _seriesSortOption);

                _lastBatchSize = series.Count;
                _lastIndex = _lastIndex + _lastBatchSize;

                foreach (var item in series)
                {
                    Add(item);
                }

                return new LoadMoreItemsResult { Count = (uint)_lastBatchSize };
            });
        }
    }
}
