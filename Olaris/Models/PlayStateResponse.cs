﻿namespace Olaris.Models
{
    public class PlayStateResponse
    {
        public string Uuid { get; set; }
        public PlayState PlayState { get; set; }
    }
}
