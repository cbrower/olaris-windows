﻿namespace Olaris.Models
{
    public enum SeriesSort
    {
        [DisplayName("Name"), DataSourceName("name")]
        Name,

        [DisplayName("Original Name"), DataSourceName("originalName")]
        OriginalName,

        [DisplayName("First Air Date"), DataSourceName("firstAirDate")]
        FirstAirDate
    }

    public class SeriesSortOption
    {
        public SeriesSort SeriesSort { get; set; }
        public SortDirection SortDirection { get; set; }

        public string DisplayString
        {
            get
            {
                return string.Format("{0}, {1}", SeriesSort.GetDisplayName(), SortDirection);
            }
        }
    }
}
