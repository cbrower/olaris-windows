﻿namespace Olaris.Models
{
    public enum SortDirection
    {
        [DisplayName("Asc"), DataSourceName("asc")]
        Asc,

        [DisplayName("Desc"), DataSourceName("desc")]
        Desc
    }
}
