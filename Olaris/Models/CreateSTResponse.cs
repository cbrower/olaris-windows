﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Olaris.Models
{
    /// <summary>
    /// The response structure to a request for a new streaming ticket.
    /// </summary>
    public class CreateSTResponse
    {
        public Error Error { get; set; }
        public string MetadataPath { get; set; }
        public string HlsStreamingPath { get; set; }
        public string DashStreamingPath { get; set; }
        public string Jwt { get; set; }
        public List<Stream> Streams { get; set; }
    }
}
