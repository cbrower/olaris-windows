﻿using LiteDB;

namespace Olaris.Models
{
    public class ApplicationSettings
    {
        public ObjectId ApplicationSettingsId { get; set; }

        /// <summary>
        /// The ID of the default Olaris server.
        /// </summary>
        public ObjectId DefaultServer { get; set; }

        /// <summary>
        /// Volume setting for the media player. Range can be 0-1.
        /// </summary>
        public double Volume { get; set; }

        /// <summary>
        /// Sets the amount of blur applied to background images.
        /// </summary>
        public double BackdropImageBlurAmount { get; set; }

        public ApplicationSettings()
        {
            ApplicationSettingsId = ObjectId.NewObjectId();
            Volume = 1;
            BackdropImageBlurAmount = 0;
        }
    }
}
