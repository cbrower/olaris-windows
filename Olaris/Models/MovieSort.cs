﻿namespace Olaris.Models
{
    public enum MovieSort
    {
        [DisplayName("Title"), DataSourceName("title")]
        Title,

        [DisplayName("Original Title"), DataSourceName("name")]
        OriginalTitle,

        [DisplayName("Release Date"), DataSourceName("releaseDate")]
        ReleaseDate
    }

    public class MovieSortOption
    {
        public MovieSort MovieSort { get; set; }
        public SortDirection SortDirection { get; set; }

        public string DisplayString
        {
            get
            {
                return string.Format("{0}, {1}", MovieSort.GetDisplayName(), SortDirection);
            }
        }
    }
}
