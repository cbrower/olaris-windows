﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Olaris.Models
{
    // An IMediaItem represents some kind of media on the server, either a movie or a TV episode.
    public class IMediaItem
    {
    }

    /// <summary>
    /// A tempalte selector for when you have a list/grid view of IMediaItem objects to display.
    /// </summary>
    public class IMediaItemDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate EpisodeTemplate { get; set; }
        public DataTemplate MovieTemplate { get; set; }

        protected override DataTemplate SelectTemplateCore(object item)
        {
            if (item is Movie)
                return MovieTemplate;
            if (item is Episode)
                return EpisodeTemplate;

            return base.SelectTemplateCore(item);
        }

        protected override DataTemplate SelectTemplateCore(object item, DependencyObject container)
        {
            return SelectTemplateCore(item);
        }
    }
}
