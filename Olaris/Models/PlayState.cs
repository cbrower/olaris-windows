﻿using System;

namespace Olaris.Models
{
    public class PlayState
    {
        public bool Finished { get; set; }
        public float PlayTime { get; set; }
        public string Uuid { get; set; }

        /// <summary>
        /// Indicates if the playstate is in a state it makes sense to resume from
        /// </summary>
        public bool CanResume
        {
            get
            {
                if (Finished == false && PlayTime > 0)
                {
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Return the PlayTime converted to a TimeSpan type
        /// </summary>
        public TimeSpan AsTimeSpan()
        {
            var playTimeSeconds = (int)Math.Round(PlayTime);
            return new TimeSpan(0, 0, playTimeSeconds);
        }
    }
}
