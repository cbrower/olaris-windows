﻿using Flurl;
using Flurl.Http;
using LiteDB;
using System;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.DataProtection;
using Windows.Storage.Streams;

namespace Olaris.Models
{
    public class OlarisServer
    {
        public ObjectId OlarisServerId { get; set; }
        public string Name { get; set; }
        public string HostName { get; set; }
        public int Port { get; set; }
        public bool UseTls { get; set; }
        public string PathPrefix { get; set; }
        public string UserName { get; set; }
        public byte[] EncryptedPassword { get; set; }

        /// <summary>
        /// Constructor that returns an empty OlarisServer with only an Id
        /// </summary>
        public OlarisServer()
        {
            OlarisServerId = ObjectId.NewObjectId();
        }

        /// <summary>
        /// Constructor that takes a full URL and parses the individual
        /// properties out. Can throw an exception if the URL is not valid.
        /// </summary>
        /// <param name="address"></param>
        /// <param name="userName"></param>
        public OlarisServer(string address, string userName, string name)
        {
            OlarisServerId = ObjectId.NewObjectId();
            Name = name?.Trim();
            UserName = userName;
            ParseUrl(address);
        }

        /// <summary>
        /// Convenience property for when the base URL needs to be displayed
        /// in the UI.
        /// </summary>
        [BsonIgnore]
        public Url BaseUrl
        {
            get
            {
                return BuildBaseUrl();
            }
        }

        [BsonIgnore]
        public Url GraphQLEndPoint
        {
            get
            {
                return BaseUrl?.AppendPathSegment("/olaris/m/query");
            }
        }

        [BsonIgnore]
        public Url AuthEndPoint
        {
            get
            {
                return BaseUrl?.AppendPathSegment("/olaris/m/v1/auth");
            }
        }

        [BsonIgnore]
        public Url VersionEndPoint
        {
            get => BaseUrl?.AppendPathSegment("/olaris/m/v1/version");
        }

        [BsonIgnore]
        public string NameOrHostName
        {
            get => string.IsNullOrEmpty(Name) ? HostName : Name;
        }

        /// <summary>
        /// Parses a string into a URL and sets the values of each component of
        /// the OlarisServer.
        /// 
        /// WARNING: This will throw an exception if a bad URL is provided.
        /// </summary>
        public void ParseUrl(string url)
        {
            var serverUrl = new Url(url);

            HostName = serverUrl.Host;
            Port = serverUrl.Port ?? 0;
            UseTls = serverUrl.IsSecureScheme;
            PathPrefix = serverUrl.Path;
        }

        /// <summary>
        /// Builds the base URL out of the components in this OlarisServer.
        /// </summary>
        /// <returns></returns>
        public Url BuildBaseUrl()
        {
            if (string.IsNullOrEmpty(HostName))
            {
                return null;
            }

            var urlBase = "";
            if (UseTls) { urlBase += "https://"; }
            else { urlBase += "http://"; }

            urlBase += HostName;

            if (Port != 0)
            {
                urlBase += ":" + Port.ToString();
            }

            var url = new Url(urlBase);

            if (PathPrefix != null && PathPrefix != "")
            {
                url.AppendPathSegment(PathPrefix);
            }

            return url;
        }

        /// <summary>
        /// Uses the Windows data protection API to encrypt the password value
        /// and store the bytes in the EncryptedPassword property.
        /// </summary>
        public async Task SetPassword(string password)
        {
            if (password == null)
            {
                EncryptedPassword = null;
                return;
            }

            // The password should be accessible locally, only by this user
            string strDescriptor = "LOCAL=user";
            BinaryStringEncoding encoding = BinaryStringEncoding.Utf8;

            DataProtectionProvider provider = new DataProtectionProvider(strDescriptor);
            IBuffer buffMsg = CryptographicBuffer.ConvertStringToBinary(password, encoding);
            IBuffer buffProtected = await provider.ProtectAsync(buffMsg);

            EncryptedPassword = buffProtected.ToArray();
            return;
        }

        /// <summary>
        /// Uses the Windows data protection API to decrypt the encrypted
        /// password and returns it as a string.
        /// </summary>
        public async Task<string> GetPassword()
        {
            if (EncryptedPassword == null)
            {
                return null;
            }

            BinaryStringEncoding encoding = BinaryStringEncoding.Utf8;

            DataProtectionProvider provider = new DataProtectionProvider();
            IBuffer buffUnprotected = await provider.UnprotectAsync(EncryptedPassword.AsBuffer());
            string strClearText = CryptographicBuffer.ConvertBinaryToString(encoding, buffUnprotected);

            return strClearText;
        }

        // Tests to see if a password is able to make a successful call to the auth endpoint. The goal here is to see if
        // the password is good, not to get a token to use anywhere.
        public async Task<bool> TestCredentials(string password)
        {
            if (AuthEndPoint == null)
            {
                throw new ApplicationException("can't build auth endpoint URL");
            }

            var response = await AuthEndPoint.AllowAnyHttpStatus().PostJsonAsync(new
            {
                username = UserName,
                password = password
            });

            if (response.StatusCode == 200)
            {
                return true;
            }

            return false;
        }

        public async Task<string> NewToken()
        {
            if (AuthEndPoint == null)
            {
                throw new ApplicationException("can't build auth endpoint URL");
            }

            var p = await GetPassword();
            if (p == null)
            {
                throw new ApplicationException("Olaris server password is null");
            }

            var resp = await AuthEndPoint.PostJsonAsync(new
            {
                username = UserName,
                password = p
            }).ReceiveJson<AuthResponse>();

            return resp.Jwt;
        }

        /// <summary>
        /// Gets the Olaris server version from the server's API. Can throw an
        /// exception if the server can't be reached.
        /// </summary>
        /// <returns></returns>
        public async Task<string> GetVersion()
        {
            if (VersionEndPoint == null)
            {
                throw new ApplicationException("can't build version endpoint URL");
            }

            var response = await VersionEndPoint.GetStringAsync();
            return response;
        }

        /// <summary>
        /// Takes a partial image path (returned from the graphQL endpoint) and turns it into a full URL
        /// </summary>
        /// <param name="imagePath"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public Url GetFullImagePath(string imagePath, PosterSize size)
        {
            var urlPath = string.Format("/olaris/m/images/tmdb/{0}", size.ToString());
            return BaseUrl?.AppendPathSegment(urlPath).AppendPathSegment(imagePath);
        }

        public Url GetFullImagePath(string imagePath, BackdropSize size)
        {
            var urlPath = string.Format("/olaris/m/images/tmdb/{0}", size.ToString());
            return BaseUrl?.AppendPathSegment(urlPath).AppendPathSegment(imagePath);
        }

        public Url GetFullImagePath(string imagePath, StillSize size)
        {
            var urlPath = string.Format("/olaris/m/images/tmdb/{0}", size.ToString());
            return BaseUrl?.AppendPathSegment(urlPath).AppendPathSegment(imagePath);
        }
    }
}
