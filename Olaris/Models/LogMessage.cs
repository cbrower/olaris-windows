﻿using System;
using LiteDB;

namespace Olaris.Models
{
    /// <summary>
    /// A log message stored in the local cache.
    /// </summary>
    public class LogMessage
    {
        public ObjectId LogMessageId { get; set; }
        
        /// <summary>
        /// The timestamp of the message
        /// </summary>
        public DateTime TimeStamp { get; set; }
        
        /// <summary>
        /// The human readable error message
        /// </summary>
        public string Message { get; set; }
        
        /// <summary>
        /// The exception's stack trace
        /// </summary>
        public string StackTrace { get; set; }
        
        /// <summary>
        /// The type of exception. It's a string because it's going to be stored
        /// in the cache for reviewing later.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Captures the page this error occurred on.
        /// </summary>
        public string Page { get; set; }

        /// <summary>
        /// The server URL that was active at the time of the error. We store
        /// the actual URL instead of the server ID because servers can be
        /// changed or deleted, which could make the log inaccurate.
        /// </summary>
        public string ServerUrl { get; set; }

        /// <summary>
        /// A user-friendly (non-technical) error message
        /// </summary>
        public string FriendlyMessage { get; set; }
        
        public LogMessage()
        {
            LogMessageId = ObjectId.NewObjectId();
        }

        /// <summary>
        /// Computed property that combines the error type and message.
        /// </summary>
        public string TypeAndMessage
        {
            get
            {
                return string.Format("{0}: {1}", Type, Message);
            }
        }
    }
}