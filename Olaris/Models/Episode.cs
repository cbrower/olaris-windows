﻿using System.Collections.Generic;
using System.Linq;

namespace Olaris.Models
{
    public class Episode : IMediaItem, ICardItem
    {
        public string Name { get; set; }
        public string Uuid { get; set; }
        public string Overview { get; set; }
        public int TmdbId { get; set; }
        public PlayState PlayState { get; set; }
        public string StillPath { get; set; }
        public string AirDate { get; set; }
        public int EpisodeNumber { get; set; }
        public List<EpisodeFile> Files { get; set; }
        public Season Season { get; set; }

        /// <summary>
        /// Calculates the progress as a double, where 0 = 0%, and 1 = 100%.
        /// </summary>
        public double Progress
        {
            get
            {
                if (Files.Count < 1 || Files.First().TotalDuration == 0) return 0;
                return PlayState.PlayTime / Files.First().TotalDuration;
            }
        }

        /// <summary>
        /// Converts the image paths for this Movie instance to absolute URLs based on a particular Olaris server.
        /// </summary>
        /// <param name="server"></param>
        public void ConvertToAbsoluteImagePaths(OlarisServer server)
        {
            if (!string.IsNullOrEmpty(StillPath)) StillPath = server.GetFullImagePath(StillPath, StillSize.original);
            if (Season != null) Season.ConvertToAbsoluteImagePaths(server);
        }

        #region Implementation: ICardItem

        public int CardCount
        {
            get => Files.Count;
        }

        public int CardMinCount
        {
            get => 2;
        }
        public string CardPosterPath
        {
            get => Season.PosterPath;
        }
        public string CardTitle
        {
            get => Name;
        }
        public string CardSubtitle
        {
            get => string.Format("S{0} E{1}", Season.SeasonNumber, EpisodeNumber);
        }
        public bool CardWatched
        {
            get => PlayState.Finished;
        }
        public double CardProgress
        {
            get => Progress;
        }

        #endregion
    }
}
