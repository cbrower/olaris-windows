﻿namespace Olaris.Models
{
    /// <summary>
    /// Fields required to render an item on a card.
    /// </summary>
    public interface ICardItem
    {
        int CardCount { get; }
        int CardMinCount { get; }
        string CardPosterPath { get; }
        string CardTitle { get; }
        string CardSubtitle { get; }
        bool CardWatched { get; }
        double CardProgress { get; }
    }
}
