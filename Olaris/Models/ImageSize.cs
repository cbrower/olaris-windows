﻿using System;

namespace Olaris.Models
{
    public enum PosterSize
    {
        w342,
        w500,
        w780,
        original
    }

    public static class PosterSizeMethods
    {
        public static string ToString(this PosterSize size)
        {
            switch (size)
            {
                case PosterSize.w342:
                    return "w342";
                case PosterSize.w500:
                    return "w500";
                case PosterSize.w780:
                    return "w780";
                case PosterSize.original:
                    return "original";
                default:
                    throw new ApplicationException("invalid PosterSize type");
            }
        }
    }

    public enum BackdropSize
    {
        w300,
        w780,
        w1280,
        original
    }

    public static class BackdropSizeMethods
    {
        public static string ToString(this BackdropSize size)
        {
            switch (size)
            {
                case BackdropSize.w300:
                    return "w300";
                case BackdropSize.w780:
                    return "w780";
                case BackdropSize.w1280:
                    return "w1280";
                case BackdropSize.original:
                    return "original";
                default:
                    throw new ApplicationException("invalid BackdropSize type");
            }
        }
    }

    public enum StillSize
    {
        w185,
        w300,
        original
    }

    public static class StillSizeMethods
    {
        public static string ToString(this StillSize size)
        {
            switch (size)
            {
                case StillSize.w185:
                    return "w185";
                case StillSize.w300:
                    return "w300";
                case StillSize.original:
                    return "original";
                default:
                    throw new ApplicationException("invalid StillSize type");
            }
        }
    }
}
