﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Olaris.Models
{
    /// <summary>
    /// A search item can either be a series or a movie. I couldn't think of a better way to represent this in C#.
    /// </summary>
    public interface ISearchItem
    {
    }

    /// <summary>
    /// A tempalte selector for when you have a list/grid view of ISearchItem objects to display.
    /// </summary>
    public class ISearchItemDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate SeriesTemplate { get; set; }
        public DataTemplate MovieTemplate { get; set; }

        protected override DataTemplate SelectTemplateCore(object item)
        {
            if (item is Movie)
                return MovieTemplate;
            if (item is Series)
                return SeriesTemplate;

            return base.SelectTemplateCore(item);
        }

        protected override DataTemplate SelectTemplateCore(object item, DependencyObject container)
        {
            return SelectTemplateCore(item);
        }
    }
}
