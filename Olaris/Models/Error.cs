﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Olaris.Models
{
    /// <summary>
    /// An error returned from the GraphQL server. This is a JSON structure, not a C# exception type.
    /// </summary>
    public class Error
    {
        public string Message { get; set; }
        public bool HasError { get; set; }
    }
}
