﻿using Olaris.Data;
using Olaris.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Windows.Security.Credentials;

namespace Olaris.Migrations
{
    internal class Migrations
    {
        private readonly List<Migration> _migrations;
        private ApplicationState _state;

        public Migrations(ApplicationState applicationState)
        {
            _state = applicationState;

            _migrations = new List<Migration>();
            RegisterAll();
        }

        /// <summary>
        /// This is pretty ugly... Register each migration by calling a method
        /// that creates it. I think it will work, but there is lots of room
        /// for improvement.
        /// </summary>
        private void RegisterAll()
        {
            _migrations.Add(Migration_001());
        }

        /// <summary>
        /// Run all migrations that haven't been run yet.
        /// </summary>
        public async Task RunAll()
        {
            foreach (var migration in _migrations)
            {
                // If it's in the database, it's already been run
                var savedMigration = _state.LocalCache.GetMigrationById(migration.Id);
                if (savedMigration != null)
                {
                    continue;
                }

                // Apply the migration and save it to the database
                await migration.Up();
                migration.AppliedAt = System.DateTime.Now;
                _state.LocalCache.InsertMigration(migration);
            }
        }

        /// <summary>
        /// Migrates server credentials from the credential locker to the
        /// local database. They are still encrypted using the Data Protection
        /// API that the credential locker uses.
        /// </summary>
        private Migration Migration_001()
        {
            return new Migration
            {
                Id = 1,
                Up = async () =>
                {
                    var vault = new PasswordVault();
                    var credentials = vault.RetrieveAll();
                    var servers = _state.LocalCache.GetOlarisServers();
                    var defaultServerId = _state.LocalCache.ApplicationSettings.DefaultServer;

                    // For each credential, if a matching Olaris server exists,
                    // set the password and remove from the vault.
                    foreach (var credential in credentials)
                    {
                        var server = servers.FirstOrDefault(item => item.BaseUrl == credential.Resource && item.UserName == credential.UserName);
                        if (server == null)
                        {
                            vault.Remove(credential);
                            continue;
                        }

                        credential.RetrievePassword();

                        await server.SetPassword(credential.Password);
                        _state.LocalCache.SaveOlarisServer(server);
                        vault.Remove(credential);
                    }

                    // Now remove olaris servers with missing passwords
                    foreach (var server in servers)
                    {
                        var p = await server.GetPassword();
                        if (string.IsNullOrEmpty(p))
                        {
                            _state.LocalCache.RemoveOlarisServer(server.OlarisServerId);
                        }
                    }

                    // If the previous default server is gone, choose a new one if possible
                    if (defaultServerId != null && _state.LocalCache.GetOlarisServerById(defaultServerId) == null)
                    {
                        servers = _state.LocalCache.GetOlarisServers();
                        _state.LocalCache.SetDefaultOlarisServer(servers.FirstOrDefault()?.OlarisServerId);
                    }

                    // Now the default server needs to be reloaded so it has a password
                    var defaultServer = _state.LocalCache.GetDefaultOlarisServer();
                    if (defaultServer == null)
                    {
                        _state.DataSource = null;
                    }
                    else
                    {
                        _state.DataSource = new GraphQLSource(defaultServer);
                    }
                        
                    return;
                }
            };
        }
    }
}
