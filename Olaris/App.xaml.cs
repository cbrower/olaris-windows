﻿using System;
using Olaris.Views;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Windows.ApplicationModel.Core;
using Windows.UI.ViewManagement;
using Windows.UI;
using Olaris.Views.Layouts;
using Windows.UI.Xaml.Media.Imaging;
using Olaris.Models;
using System.Threading.Tasks;

namespace Olaris
{

    /// <summary>
    /// App is the top-level application as a whole - more or less.
    /// </summary>
    sealed partial class App : Application
    {
        #region Properties

        /// <summary>
        /// Shared state data that is commonly accessed throughout the app.
        /// </summary>
        public ApplicationState State { get; }

        #endregion

        #region Events

        /// <summary>
        /// Set the app's backdrop image, if applicable.
        /// </summary>
        public event EventHandler<BitmapImage> BackdropRequested;

        /// <summary>
        /// Event is fired when the active Olaris server is changed.
        /// </summary>
        public event EventHandler<OlarisServer> ServerChanged;

        /// <summary>
        /// Event is fired when the application settings are modified at
        /// runtime.
        /// </summary>
        public event EventHandler<ApplicationSettings> ApplicationSettingsChanged;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes the singleton application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            Suspending += OnSuspending;
            State = new ApplicationState();
            InitializeApp();
            
        }

        public async Task InitializeApp()
        {
            var migrations = new Migrations.Migrations(State);
            await migrations.RunAll();

            State.LocalCache.PruneOldLogMessages();
            State.LocalCache.ApplicationSettingsChanged += LocalCache_ApplicationSettingsChanged;

            if (State.DataSource != null)
            {
                NotifyServerChanged(State.LocalCache.GetOlarisServerById(State.DataSource.ServerId));
            }

            InitializeComponent();
        }

        #endregion

        #region Methods - Life Cycle

        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used such as when the application is launched to open a specific file.
        /// </summary>
        /// <param name="e">Details about the launch request and process.</param>
        protected override void OnLaunched(LaunchActivatedEventArgs e)
        {
            Frame rootFrame = Window.Current.Content as Frame;

            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            if (rootFrame == null)
            {
                // Create a Frame to act as the navigation context and navigate to the first page
                rootFrame = new Frame();
                rootFrame.NavigationFailed += OnNavigationFailed;

                if (e.PreviousExecutionState == ApplicationExecutionState.Terminated)
                {
                    //TODO: Load state from previously suspended application
                }

                // Place the frame in the current Window
                Window.Current.Content = rootFrame;
            }

            if (e.PrelaunchActivated == false)
            {
                if (rootFrame.Content == null)
                {
                    // When the navigation stack isn't restored navigate to the first page,
                    // configuring the new page by passing required information as a navigation
                    // parameter
                    if (State.DataSource != null) { GoToMainPage(); }
                    else { GoToLogin(); }
                }

                // Ensure the current window is active
                Window.Current.Activate();
            }

            CoreApplication.GetCurrentView().TitleBar.ExtendViewIntoTitleBar = true;

            var appView = ApplicationView.GetForCurrentView();
            appView.TitleBar.ButtonBackgroundColor = Colors.Transparent;
            appView.TitleBar.ButtonInactiveBackgroundColor = Colors.Transparent;
            appView.TitleBar.ButtonHoverBackgroundColor = (Color)Application.Current.Resources["OlarisOrange"];
            appView.TitleBar.ButtonPressedBackgroundColor = (Color)Application.Current.Resources["OlarisOrange_Lighter"];
        }

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            //TODO: Save application state and stop any background activity
            deferral.Complete();
        }

        #endregion

        #region Methods - Navigation

        /// <summary>
        /// Invoked when Navigation to a certain page fails
        /// </summary>
        /// <param name="sender">The Frame which failed navigation</param>
        /// <param name="e">Details about the navigation failure</param>
        void OnNavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            throw new Exception("Failed to load Page " + e.SourcePageType.FullName);
        }

        public void GoToMainPage()
        {
            Frame rootFrame = Window.Current.Content as Frame;
            rootFrame.Navigate(typeof(MainLayout));
        }

        /// <summary>
        /// Go to the login page
        /// </summary>
        public void GoToLogin()
        {
            Frame rootFrame = Window.Current.Content as Frame;
            rootFrame.Navigate(typeof(Login));
        }

        /// <summary>
        /// Go to the player view and play the specified media item.
        /// </summary>
        /// <param name="options">The options for the player.</param>
        public void GoToPlayer(Player.Options options)
        {
            Frame rootFrame = Window.Current.Content as Frame;
            rootFrame.Navigate(typeof(Player), options);
        }

        #endregion

        #region Methods - Events

        /// <summary>
        /// Used when a component wants to request a different backdrop image.
        /// </summary>
        /// <param name="image"></param>
        public void RequestBackdrop(BitmapImage image)
        {
            BackdropRequested?.Invoke(this, image);
        }

        /// <summary>
        /// Creates a bitmap image from a URL string and requests it as a
        /// backdrop. Prevents this code from being duplicated more than
        /// necessary.
        /// </summary>
        /// <param name="path"></param>
        public void RequestBackdropFromUri(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                BackdropRequested?.Invoke(this, null);
                return;
            }

            try
            {
                var image = new BitmapImage(new Uri(path));
                BackdropRequested?.Invoke(this, image);
            }
            catch (Exception _)
            {
                // TODO: log this error?
            }
        }

        /// <summary>
        /// Requests the removal of any current backdrop.
        /// </summary>
        public void ClearBackdrop()
        {
            BackdropRequested?.Invoke(this, null);
        }

        /// <summary>
        /// Notifies any listeners that the active server has changed.
        /// </summary>
        /// <param name="server"></param>
        public void NotifyServerChanged(OlarisServer server)
        {
            ServerChanged?.Invoke(this, server);
        }

        /// <summary>
        /// Proxy the event so that views can listen for it on the app instance.
        /// </summary>
        private void LocalCache_ApplicationSettingsChanged(object sender, ApplicationSettings e)
        {
            ApplicationSettingsChanged?.Invoke(this, e);
        }

        #endregion
    }
}