﻿using Flurl;
using GraphQL.Client.Http;
using GraphQL.Client.Serializer.Newtonsoft;
using LiteDB;
using Olaris.Models;
using Olaris.Streaming;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Olaris.Data
{
    // Implements the official GraphQL client interface for Olaris as an IDataSource
    public partial class GraphQLSource : IDataSource
    {
        private OlarisServer _olarisServer;
        private JwtSecurityToken _jwt;

        public GraphQLSource(OlarisServer server)
        {
            _olarisServer = server;
        }

        public ObjectId ServerId
        {
            get => _olarisServer.OlarisServerId;
        }

        // Creates a new GraphQL client WITHOUT auth headers
        private GraphQLHttpClient NewClient()
        {
            return new GraphQLHttpClient(_olarisServer.GraphQLEndPoint, new NewtonsoftJsonSerializer());
        }

        /// <summary>
        /// Updates the JWT for this data source
        /// </summary>
        /// <returns></returns>
        private async Task UpdateTokenIfRequired()
        {
            // If there is no token, get one
            if (_jwt == null)
            {
                await UpdateToken();
                return;
            }

            // If the token is expired, get  new one
            if (_jwt.ValidTo.CompareTo(DateTime.Now) <= 0)
            {
                await UpdateToken();
                return;
            }
        }

        /// <summary>
        /// Updates the JWT for this data source
        /// </summary>
        /// <returns></returns>
        private async Task UpdateToken()
        {
            var tokenString = await _olarisServer.NewToken();
            _jwt = new JwtSecurityToken(tokenString);
        }

        // Creates a new GraphQL client WITH auth headers
        private async Task<GraphQLHttpClient> NewAuthenticatedClient()
        {
            await UpdateTokenIfRequired();
            var options = new GraphQLHttpClientOptions
            {
                EndPoint = new Uri(_olarisServer.GraphQLEndPoint)
            };

            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _jwt.RawData);
            return new GraphQLHttpClient(options, new NewtonsoftJsonSerializer(), httpClient);
        }

        /// <summary>
        /// Creates a streaming session for the media item identified by the
        /// given UUID.
        /// </summary>
        public async Task<Session> CreateStreamingSession(string uuid)
        {
            var streamingTicket = await CreateStreamingTicket(uuid);
            return new Session(_olarisServer, streamingTicket);
        }
    }
}
