﻿using GraphQL;
using Newtonsoft.Json;
using Olaris.Models;
using System.Threading.Tasks;

namespace Olaris.Data
{
    public partial class GraphQLSource
    {

        public class EpisodeResponse
        {
            [JsonProperty("episode")]
            public Episode Episode { get; set; }
        }

        public async Task<Episode> GetEpisodeById(string uuid)
        {
            var client = await NewAuthenticatedClient();

            var request = new GraphQLRequest
            {
                Query = @"
                query episode($uuid: String!) {
                    episode(uuid: $uuid) {
                        type: __typename
                        name
                        overview
                        airDate
                        stillPath
                        uuid
                        episodeNumber

                        season {
                            uuid
                            name
                            seasonNumber
                            episodes {
                                episodeNumber
                                uuid
                            }
                            series {
                                uuid
                                name
                                posterPath
                                backdropPath
                            }
                        }

                        playState {
                            finished
                            playtime
                        }

                        files {
                            fileName
                            filePath
                            fileSize
                            uuid
                            totalDuration
                            library {
                                healthy
                            }
                            streams {
                                streamType
                                codecMime
                                codecName
                                resolution
                                bitRate
                                language
                                profile
                            }
                        }
                    }
                }",
                OperationName = "episode",
                Variables = new
                {
                    uuid = uuid
                }
            };

            var response = await client.SendQueryAsync<EpisodeResponse>(request);
            response.Data.Episode.ConvertToAbsoluteImagePaths(_olarisServer);
            return response.Data.Episode;
        }

    }
}
