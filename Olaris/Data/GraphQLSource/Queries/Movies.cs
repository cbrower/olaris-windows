﻿using GraphQL;
using Newtonsoft.Json;
using Olaris.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Olaris.Data
{
    public partial class GraphQLSource
    {

        public class MovieResponse
        {
            [JsonProperty("movies")]
            public List<Movie> Movies { get; set; }
        }

        public async Task<Movie> GetMovieById(string uuid)
        {
            var client = await NewAuthenticatedClient();

            var request = new GraphQLRequest
            {
                Query = @"
                query movies($uuid: String!) {
                    movies(uuid: $uuid) {
                        type: __typename
                        name
                        title
                        year
                        overview
                        imdbID
                        backdropPath
                        uuid
                        posterPath

                        playState {
                            finished
                            playtime
                        }

                        files {
                            fileName
                            filePath
                            uuid
                            totalDuration
                            library {
                                healthy
                            }
                            streams {
                                codecMime
                                streamType
                                resolution
                                bitRate
                                language
                            }
                        }
                    }
                }",
                OperationName = "movies",
                Variables = new
                {
                    uuid = uuid
                }
            };

            var response = await client.SendQueryAsync<MovieResponse>(request);
            if (response.Data.Movies.Count < 1)
            {
                // TODO: make real exceptions
                throw new ApplicationException("movie not found");
            }

            var movie = response.Data.Movies[0];
            movie.ConvertToAbsoluteImagePaths(_olarisServer);

            return movie;
        }

        /// <summary>
        /// Returns up to $limit movies, starting at $offset
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public async Task<List<Movie>> GetMovies(int offset, int limit, MovieSortOption movieSortOption)
        {
            var client = await NewAuthenticatedClient();

            var request = new GraphQLRequest
            {
                Query = @"
                query movies($offset: Int, $limit: Int, $sort: MovieSort, $sortDirection: SortDirection) {
                    movies(offset: $offset, limit: $limit, sort: $sort, sortDirection: $sortDirection) {
                        type: __typename
                        title
                        posterPath
                        uuid
                        year

                        playState {
                            finished
                            playtime
                        }

                        files {
                            totalDuration
                        }
                    }
                }",
                OperationName = "movies",
                Variables = new
                {
                    offset = offset,
                    limit = limit,
                    sort = movieSortOption.MovieSort.GetDataSourceName(),
                    sortDirection = movieSortOption.SortDirection.GetDataSourceName()
                }
            };

            var response = await client.SendQueryAsync<MovieResponse>(request);
            foreach (var item in response.Data.Movies)
            {
                item.ConvertToAbsoluteImagePaths(_olarisServer);
            }

            return response.Data.Movies;
        }

    }
}
