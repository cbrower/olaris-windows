﻿using GraphQL;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Olaris.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Olaris.Data
{
    public partial class GraphQLSource
    {
        /// <summary>
        /// Data structure of the search graphql query response.
        /// </summary>
        public class SearchResponse
        {
            [JsonProperty("search")]
            public List<JObject> SearchResults { get; set; }
        }

        /// <summary>
        /// Gets the search results for the query from the server.
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        public async Task<List<ISearchItem>> GetSearchResults(string query)
        {
            var client = await NewAuthenticatedClient();

            var request = new GraphQLRequest
            {
                Query = @"
                query search($value: String!) {
                    search(name: $value) {
                        type: __typename
                        ... on Movie {
                            title
                            posterPath
                            year
                            uuid

                            playState {
                                finished
                                playtime
                            }

                            files {
                                totalDuration
                            }
                        }
                        ... on Series {
                            name
                            posterPath
                            firstAirDate
                            uuid
                            unwatchedEpisodesCount
                            seasons {
                                uuid
                            }
                        }
                    }
                }",
                OperationName = "search",
                Variables = new
                {
                    value = query
                }
            };

            var response = await client.SendQueryAsync<SearchResponse>(request);

            var searchResults = new List<ISearchItem>();

            foreach (JObject item in response.Data.SearchResults)
            {
                if (item.Value<string>("type") == "Movie")
                {
                    var thisMovie = item.ToObject<Movie>();
                    thisMovie.ConvertToAbsoluteImagePaths(_olarisServer);
                    searchResults.Add(thisMovie);
                }
                if (item.Value<string>("type") == "Series")
                {
                    var thisSeries = item.ToObject<Series>();
                    thisSeries.ConvertToAbsoluteImagePaths(_olarisServer);
                    searchResults.Add(thisSeries);
                }
            }

            return searchResults;
        }

    }
}
