﻿using GraphQL;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Olaris.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Olaris.Data
{
    public partial class GraphQLSource
    {

        public class UpNextResponse
        {
            [JsonProperty("upNext")]
            public List<JObject> UpNext { get; set; }
        }

        public async Task<List<IMediaItem>> GetUpNextMediaItems()
        {
            var client = await NewAuthenticatedClient();

            var request = new GraphQLRequest
            {
                Query = @"
                {
                    upNext {
                        ... on Movie {
                            type: __typename
                            uuid
                            title
                            year
                            posterPath

                            playState {
                                finished
                                playtime
                            }

                            files {
                                totalDuration
                            }
                        }
                        ... on Episode {
                            type: __typename
                            uuid
                            name
                            episodeNumber

                            season {
                                seasonNumber
                                posterPath
                                uuid

                                series {
                                    uuid
                                    name
                                    posterPath
                                }
                            }

                            playState {
                                finished
                                playtime
                            }

                            files {
                                totalDuration
                            }
                        }
                    }
                }"
            };

            var response = await client.SendQueryAsync<UpNextResponse>(request);

            var upNext = new List<IMediaItem>();

            foreach (JObject item in response.Data.UpNext)
            {
                if (item.Value<string>("type") == "Movie")
                {
                    var thisMovie = item.ToObject<Movie>();
                    thisMovie.ConvertToAbsoluteImagePaths(_olarisServer);
                    upNext.Add(thisMovie);
                }
                if (item.Value<string>("type") == "Episode")
                {
                    var thisEpisode = item.ToObject<Episode>();
                    thisEpisode.ConvertToAbsoluteImagePaths(_olarisServer);
                    upNext.Add(thisEpisode);
                }
            }

            return upNext;
        }

    }
}
