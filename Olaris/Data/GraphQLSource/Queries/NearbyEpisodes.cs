﻿using GraphQL;
using Olaris.Models;
using System.Threading.Tasks;

namespace Olaris.Data
{
    public partial class GraphQLSource
    {
        public class NearbyEpisodesResponseData
        {
            public NearbyEpisodesResponse NearbyEpisodes { get; set; }
        }

        /// <summary>
        /// Gets n episodes on either side of the given episode.
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public async Task<NearbyEpisodesResponse> GetNearbyEpisodes(string episodeUuid, int count)
        {
            var client = await NewAuthenticatedClient();

            var request = new GraphQLRequest
            {
                Query = @"
                query nearbyEpisodes($uuid: String!, $count: Int!) {
                    nearbyEpisodes(uuid: $uuid, previousLimit: $count, nextLimit: $count) {
                        previous {
                            uuid
                        }
                        next {
                            uuid
                        }
                    }
                }",
                OperationName = "nearbyEpisodes",
                Variables = new
                {
                    uuid = episodeUuid,
                    count = count
                }
            };

            var response = await client.SendQueryAsync<NearbyEpisodesResponseData>(request);
            return response.Data.NearbyEpisodes;
        }
    }
}
