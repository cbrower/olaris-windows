﻿using GraphQL;
using Newtonsoft.Json;
using Olaris.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Olaris.Data
{
    public partial class GraphQLSource
    {
        /// <summary>
        /// Data structure of the series graphql query response.
        /// </summary>
        public class SeriesResponse
        {
            [JsonProperty("series")]
            public List<Series> Series { get; set; }
        }

        /// <summary>
        /// Gets a series from the server by its UUID.
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        public async Task<Series> GetSeriesById(string uuid)
        {
            var client = await NewAuthenticatedClient();

            var request = new GraphQLRequest
            {
                Query = @"
                query series($uuid: String!) {
                    series(uuid: $uuid) {
                        type: __typename
                        name
                        originalName
                        uuid
                        overview
                        posterPath
                        backdropPath
                        firstAirDate
                        unwatchedEpisodesCount

                        seasons {
                            type: __typename
                            name
                            seasonNumber
                            posterPath
                            uuid
                            unwatchedEpisodesCount

                            series {
                                posterPath
                            }

                            episodes {
                                uuid

                                playState {
                                    finished
                                    playtime
                                }
                            }
                        }
                    }
                }",
                OperationName = "series",
                Variables = new
                {
                    uuid = uuid
                }
            };

            var response = await client.SendQueryAsync<SeriesResponse>(request);
            if (response.Data.Series.Count < 1)
            {
                // TODO: make real exceptions
                throw new ApplicationException("series not found");
            }

            var series = response.Data.Series[0];

            // Sort the seasons
            series.Seasons.Sort((x, y) => x.SeasonNumber - y.SeasonNumber);

            // Convert the season poster path fragments to absolute URLs
            series.ConvertToAbsoluteImagePaths(_olarisServer);

            return series;
        }

        /// <summary>
        /// Returns up to $limit series, starting at $offset
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public async Task<List<Series>> GetSeries(int offset, int limit, SeriesSortOption seriesSortOption)
        {
            var client = await NewAuthenticatedClient();

            var request = new GraphQLRequest
            {
                Query = @"
                query series($offset: Int, $limit: Int, $sort: SeriesSort, $sortDirection: SortDirection) {
                    series(offset: $offset, limit: $limit, sort: $sort, sortDirection: $sortDirection) {
                        type: __typename
                        name
                        posterPath
                        uuid
                        unwatchedEpisodesCount
                        
                        seasons {
                            uuid
                        }
                    }
                }",
                OperationName = "series",
                Variables = new
                {
                    offset = offset,
                    limit = limit,
                    sort = seriesSortOption.SeriesSort.GetDataSourceName(),
                    sortDirection = seriesSortOption.SortDirection.GetDataSourceName()
                }
            };

            var response = await client.SendQueryAsync<SeriesResponse>(request);
            foreach (var item in response.Data.Series)
            {
                item.ConvertToAbsoluteImagePaths(_olarisServer);
            }

            return response.Data.Series;
        }

    }
}
