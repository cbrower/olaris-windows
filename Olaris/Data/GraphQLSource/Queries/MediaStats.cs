﻿using GraphQL;
using Newtonsoft.Json;
using Olaris.Models;
using System.Threading.Tasks;

namespace Olaris.Data
{
    public partial class GraphQLSource
    {
        public class MediaStatsResponseData
        {
            [JsonProperty("mediaStats")]
            public MediaStatsResponse MediaStatsResponse { get; set; }
        }

        public async Task<int> GetMovieCount()
        {
            var client = await NewAuthenticatedClient();

            var request = new GraphQLRequest
            {
                Query = @"
                query mediaStats {
                    mediaStats {
                        movieCount
                    }
                }",
                OperationName = "mediaStats"
            };

            var response = await client.SendQueryAsync<MediaStatsResponseData>(request);
            return response.Data.MediaStatsResponse.MovieCount;
        }

        public async Task<int> GetSeriesCount()
        {
            var client = await NewAuthenticatedClient();

            var request = new GraphQLRequest
            {
                Query = @"
                query mediaStats {
                    mediaStats {
                        seriesCount
                    }
                }",
                OperationName = "mediaStats"
            };

            var response = await client.SendQueryAsync<MediaStatsResponseData>(request);
            return response.Data.MediaStatsResponse.SeriesCount;
        }
    }
}
