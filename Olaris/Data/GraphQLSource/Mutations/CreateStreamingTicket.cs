﻿using GraphQL;
using Newtonsoft.Json;
using Olaris.Models;
using System.Threading.Tasks;

namespace Olaris.Data
{
    public partial class GraphQLSource
    {

        public class CreateStreamingTicketResponse
        {
            [JsonProperty("createStreamingTicket")]
            public CreateSTResponse CreateStreamingTicket { get; set; }
        }

        /// <summary>
        /// Creates a new streaming ticket for the media file identified by uuid. This would be the Uuid of the
        /// MovieFile or EpisodeFile.
        /// </summary>
        /// <param name="uuid"></param>
        /// <returns></returns>
        public async Task<CreateSTResponse> CreateStreamingTicket(string uuid)
        {
            var client = await NewAuthenticatedClient();

            var request = new GraphQLRequest
            {
                Query = @"
                mutation createStreamingTicket($uuid: String!) {
                    createStreamingTicket(uuid: $uuid) {
                        error {
                            hasError
                            message
                        }
                        streams {
                            language
                            title
                            streamType
                            streamID
                            streamURL
                        }
                        metadataPath
                        dashStreamingPath
                        hlsStreamingPath
                        jwt
                    }
                }",
                
                OperationName = "createStreamingTicket",
                Variables = new
                {
                    uuid = uuid
                }
            };

            var response = await client.SendMutationAsync<CreateStreamingTicketResponse>(request);
            return response.Data.CreateStreamingTicket;
        }

    }
}
