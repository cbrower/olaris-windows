﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace Olaris.Helpers
{

    // Converts a bool to a Visibility so that it can be used to toggle a
    // control's visibility in XAML. Coverts true to Visible and false to
    // Collapsed.
    public class BoolToVisConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return (value is bool && (bool)value) ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value is Visibility && (Visibility)value == Visibility.Visible;
        }
    }

    // Converts a bool to a Visibility so that it can be used to toggle a
    // control's visibility in XAML. Coverts true to Collapsed and false to
    // Visible.
    public class BoolToVisConverterInverted : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return (value is bool && (bool)value) ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value is Visibility && (Visibility)value == Visibility.Collapsed;
        }
    }

    /// <summary>
    /// Converts a string to a Visibility. An empty or null string returns
    /// Collapsed. Going the other way just returns the ToString() for the
    /// visibility.
    /// </summary>
    public class StringToVisConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (!(value is string))
            {
                throw new ArgumentException("value is not a string");
            }

            // safe because we just threw an exception if value is not a string
            if (string.IsNullOrEmpty((string)value))
            {
                return Visibility.Collapsed;
            }

            return Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            if (!(value is Visibility))
            {
                throw new ArgumentException("value is not a visibility");
            }

            return ((Visibility)value).ToString();
        }
    }

    /// <summary>
    /// Converts a double to a Visibility. A value of 0 returns Collapsed. Going
    /// the other way returns 0 for Collapsed, and 1 for Visible.
    /// </summary>
    public class DoubleToVisConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (!(value is double))
            {
                throw new ArgumentException("value is not a double");
            }

            // safe because we just threw an exception if value is not a double
            if ((double)value == 0)
            {
                return Visibility.Collapsed;
            }

            return Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            if (!(value is Visibility))
            {
                throw new ArgumentException("value is not a visibility");
            }

            return (Visibility)value == Visibility.Visible ? (double)1 : (double)0;
        }
    }

    /// <summary>
    /// Converts an int to a Visibility. A value of 0 returns Collapsed. Going
    /// the other way returns 0 for Collapsed, and 1 for Visible.
    /// </summary>
    public class IntToVisConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (!(value is int))
            {
                throw new ArgumentException("value is not an int");
            }

            // safe because we just threw an exception if value is not a int
            if ((int)value == 0)
            {
                return Visibility.Collapsed;
            }

            return Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            if (!(value is Visibility))
            {
                throw new ArgumentException("value is not a visibility");
            }

            return (Visibility)value == Visibility.Visible ? (int)1 : (int)0;
        }
    }

    /// <summary>
    /// Converts a string to a boolean. False if the string is null/empty. True
    /// otherwise. Going in reverse, just returns "true" or "false".
    /// </summary>
    public class StringToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (!(value is string || value == null))
            {
                throw new ArgumentException("value is not a string");
            }

            return !string.IsNullOrEmpty((string)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            if (!(value is bool))
            {
                throw new ArgumentException("value is not a bool");
            }

            return (bool)value ? "true" : "false";
        }
    }
}
