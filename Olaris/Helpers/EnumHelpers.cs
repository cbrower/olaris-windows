﻿using System;
using System.Reflection;

/// <summary>
/// The display name for the enum value anywhere it is displayed to users.
/// </summary>
public class DisplayNameAttribute : Attribute
{
    /// <summary>
    /// Holds the display name for a value in an enum.
    /// </summary>
    public string DisplayName { get; protected set; }

    /// <summary>
    /// Constructor used to init a DisplayName Attribute
    /// </summary>
    /// <param name="value"></param>
    public DisplayNameAttribute(string value)
    {
        DisplayName = value;
    }
}

/// <summary>
/// The name the data source (GraphQL server) will expect for this enum value.
/// </summary>
public class DataSourceNameAttribute : Attribute
{
    /// <summary>
    /// Holds the data source name for a value in an enum.
    /// </summary>
    public string DataSourceName { get; protected set; }

    /// <summary>
    /// Constructor used to init a DataSourceName Attribute
    /// </summary>
    /// <param name="value"></param>
    public DataSourceNameAttribute(string value)
    {
        DataSourceName = value;
    }
}

public static class EnumExtensionMethods
{
    /// <summary>
    /// Gets the DisplayName property of an enum value
    /// </summary>
    public static string GetDisplayName(this Enum value)
    {
        // Get the type
        Type type = value.GetType();

        // Get fieldinfo for this type
        FieldInfo fieldInfo = type.GetField(value.ToString());

        // Get the stringvalue attributes
        DisplayNameAttribute[] attribs = fieldInfo.GetCustomAttributes(
            typeof(DisplayNameAttribute), false) as DisplayNameAttribute[];

        // Return the first if there was a match.
        return attribs.Length > 0 ? attribs[0].DisplayName : null;
    }

    /// <summary>
    /// Gets the DataSourceName property of an enum value
    /// </summary>
    public static string GetDataSourceName(this Enum value)
    {
        // Get the type
        Type type = value.GetType();

        // Get fieldinfo for this type
        FieldInfo fieldInfo = type.GetField(value.ToString());

        // Get the stringvalue attributes
        DataSourceNameAttribute[] attribs = fieldInfo.GetCustomAttributes(
            typeof(DataSourceNameAttribute), false) as DataSourceNameAttribute[];

        // Return the first if there was a match.
        return attribs.Length > 0 ? attribs[0].DataSourceName : null;
    }
}