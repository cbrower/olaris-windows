﻿using Olaris.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace Olaris.Views
{
    /// <summary>
    /// Page that displays the list of movies.
    /// </summary>
    public sealed partial class SeriesList : Page, INotifyPropertyChanged
    {
        private App _app;
        private IncrementalLoadingSeriesCollection _series;
        private int _seriesCount;
        private IList<SeriesSortOption> _sortOptions;
        private SeriesSortOption _seriesSortOption;

        /// <summary>
        /// Required for INotifyPropertyChanged
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        public SeriesList()
        {
            _app = ((App)Application.Current);
            NavigationCacheMode = NavigationCacheMode.Enabled;
            _seriesSortOption = SortOptions[0];
            ResetSeriesCollection();
            InitializeComponent();
        }

        /// <summary>
        /// Creates a new series collection to "reset" the page.
        /// </summary>
        private async Task ResetSeriesCollection()
        {
            _series = new IncrementalLoadingSeriesCollection(_app.State.DataSource, _seriesSortOption);
            NotifyPropertyChanged(nameof(_series));

            // Not awaited because we want to immediately get the series count.
            // The incremental loading collection will take care of updating
            // the UI when it loads items.
            _series.LoadMoreItemsAsync(1);

            _seriesCount = await _app.State.DataSource.GetSeriesCount();
            NotifyPropertyChanged(nameof(_seriesCount));
        }

        /// <summary>
        /// Required for INotifyPropertyChanged
        /// </summary>
        /// <param name="propertyName">
        /// Name of the parameter that changed.
        /// </param>
        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Property that returns all available SeriesSort and SortDirection
        /// combinations.
        /// </summary>
        private IList<SeriesSortOption> SortOptions
        {
            get
            {
                if (_sortOptions == null)
                {
                    var values = new List<SeriesSortOption>();
                    foreach (SeriesSort ss in Enum.GetValues(typeof(SeriesSort)))
                    {
                        foreach (SortDirection sd in Enum.GetValues(typeof(SortDirection)))
                        {
                            values.Add(new SeriesSortOption
                            {
                                SeriesSort = ss,
                                SortDirection = sd
                            });
                        }
                    }
                    _sortOptions = values;
                }

                return _sortOptions;
            }
        }

        /// <summary>
        /// On navigated to, clear any previous selections.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            _app.ClearBackdrop();
            SeriesGrid.SelectedItem = null;

            if (e.NavigationMode == NavigationMode.Back)
            {
                return;
            }

            _seriesSortOption = SortOptions[0];
            SortSelector.SelectedIndex = 0;
            ResetSeriesCollection();
        }

        /// <summary>
        /// Handle click events on the resultslist items. The item could be either a movie or a series.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SeriesGrid_ItemClick(object sender, ItemClickEventArgs e)
        {
            var selectedItem = e.ClickedItem;
            Frame.Navigate(typeof(SeriesView), (selectedItem as Series).Uuid);
        }

        /// <summary>
        /// When the sort order is changed, reset the series collection
        /// </summary>
        private void SortSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var c = sender as ComboBox;
            var newSortOption = (SeriesSortOption)c.SelectedItem;

            // Do nothing if the sort parameters have not changed. This could
            // happen the first time you open the sort dropdown box.
            if (newSortOption == _seriesSortOption)
            {
                return;
            }

            _seriesSortOption = newSortOption;
            ResetSeriesCollection();
        }
    }
}
