﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Olaris.Models;

namespace Olaris.Views
{
    /// <summary>
    /// A page that displays an exception to the user
    /// </summary>
    public sealed partial class Error : Page
    {
        private App _app;
        private Options _options;

        public Error()
        {
            _app = (App)Application.Current;
            InitializeComponent();
        }

        /// <summary>
        /// This is the type you would pass in when you navigate to this page that gives it some context and allows it
        /// to direct to a certain page.
        /// </summary>
        public class Options
        {
            public Type Page;
            public Exception Exception;
            public string Message;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            
            _app.ClearBackdrop();
            _options = e.Parameter as Options;
            var friendlyMessage = _options?.Message ?? _options?.Exception?.Message ?? "An error occurred.";
            _exceptionText.Text = friendlyMessage;



            if (_options?.Exception == null)
            {
                return;
            }

            var currentOlarisServer = _app.State.LocalCache.GetOlarisServerById(_app.State.DataSource.ServerId);

            var msg = new LogMessage()
            {
                TimeStamp = DateTime.Now,
                Message = _options.Exception.Message,
                StackTrace = _options.Exception.ToString(),
                Type = _options.Exception.GetType().ToString(),
                FriendlyMessage = friendlyMessage,
                Page = _options.Page.Name,
                ServerUrl = currentOlarisServer.BaseUrl
            };
            _app.State.LocalCache.InsertLogMessage(msg);
        }

        private void RetryButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.GoBack();
        }

        private void LogsButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Logs));
        }
    }
}
