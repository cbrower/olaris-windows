﻿using Olaris.Models;
using System.Collections.ObjectModel;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

namespace Olaris.Views.Layouts
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainLayout : Page
    {
        private App _app;

        public MainLayout()
        {
            _app = (App)Application.Current;
            _app.BackdropRequested += OnBackdropRequested;
            _app.ServerChanged += OnServerChanged;
            _app.ApplicationSettingsChanged += OnApplicationSettingsChanged;

            NavigationCacheMode = NavigationCacheMode.Enabled;
            InitializeComponent();
            SetNavigation();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            Window.Current.SetTitleBar(AppTitleBar);

            if (ContentFrame.Content is IRefreshable page)
            {
                page.Refresh();
            }

            var activeServer = _app.State.LocalCache.GetOlarisServerById(_app.State.DataSource.ServerId);
            ActiveServerLabel.Text = string.IsNullOrEmpty(activeServer.Name) ? activeServer.HostName : activeServer.Name;

            Backdrop.BlurAmount = _app.State.LocalCache.ApplicationSettings.BackdropImageBlurAmount;
        }

        /// <summary>
        /// If the content frame is null, go to the dashboard
        /// </summary>
        private void SetNavigation()
        {
            if (ContentFrame.Content == null)
            {
                ContentFrame.Navigate(typeof(Dashboard));
            }
        }

        /// <summary>
        /// Handles search query submission.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void SearchBox_QuerySubmitted(AutoSuggestBox sender, AutoSuggestBoxQuerySubmittedEventArgs args)
        {
            var query = args.QueryText;
            ContentFrame.Navigate(typeof(SearchResults), query);
        }

        /// <summary>
        /// When a different backdrop is requested, update the backdrop component.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnBackdropRequested(object sender, BitmapImage image)
        {
            Backdrop.ImageSource = image;
        }

        /// <summary>
        /// When the server is changed, update the active server indicator.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="server"></param>
        private void OnServerChanged(object sender, OlarisServer server)
        {
            ActiveServerLabel.Text = string.IsNullOrEmpty(server.Name) ? server.HostName : server.Name;
        }

        /// <summary>
        /// When the application settings are changed, update anything that
        /// needs to be updated.
        /// </summary>
        private void OnApplicationSettingsChanged(object sender, ApplicationSettings settings)
        {
            Backdrop.BlurAmount = settings.BackdropImageBlurAmount;
        }

        /// <summary>
        /// Navigate the frame back one page if possible.
        /// </summary>
        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            if (ContentFrame.CanGoBack)
            {
                ContentFrame.GoBack();
            }
        }

        /// <summary>
        /// Disable or enable the back button based on if the content frame can go back.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ContentFrame_Navigated(object sender, NavigationEventArgs e)
        {
            if (ContentFrame.CanGoBack)
            {
                BackButton.IsEnabled = true;
            }
            else
            {
                BackButton.IsEnabled = false;
            }
        }

        private void MoviesButton_Click(object sender, RoutedEventArgs e)
        {
            ContentFrame.Navigate(typeof(MovieList));
        }

        private void HomeButton_Click(object sender, RoutedEventArgs e)
        {
            ContentFrame.Navigate(typeof(Dashboard));
        }

        private void SeriesButton_Click(object sender, RoutedEventArgs e)
        {
            ContentFrame.Navigate(typeof(SeriesList));
        }

        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            ContentFrame.Navigate(typeof(SettingsView));
        }

        /// <summary>
        /// Clear any cached pages in the content frame - useful when switching
        /// servers.
        /// </summary>
        private void ResetPageCache()
        {
            var cacheSize = ContentFrame.CacheSize;
            ContentFrame.CacheSize = 0;
            ContentFrame.CacheSize = cacheSize;
        }
    }
}