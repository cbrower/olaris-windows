﻿using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;
using Olaris.Models;
using System;
using Windows.UI.Xaml.Navigation;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace Olaris.Views
{
    /// <summary>
    /// The dashboard that shows next episodes and recently added content
    /// </summary>
    public sealed partial class Dashboard : Page
    {
        private App _app;
        private ObservableCollection<IMediaItem> _upNext = new ObservableCollection<IMediaItem>();
        private ObservableCollection<IMediaItem> _recentlyAddedMovies = new ObservableCollection<IMediaItem>();
        private ObservableCollection<IMediaItem> _recentlyAddedEpisodes = new ObservableCollection<IMediaItem>();

        /// <summary>
        /// Constructor
        /// </summary>
        public Dashboard()
        {
            _app = ((App)Application.Current);
            InitializeComponent();
        }

        /// <summary>
        /// When navigated to, load the media items from the server.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            _app.ClearBackdrop();
            LoadMedia();
        }

        /// <summary>
        /// Fetches the up-next and recently added media from the server
        /// </summary>
        private async void LoadMedia()
        {
            try
            {
                var task1 = LoadUpNext();
                var task2 = LoadRecentlyAdded();
                
                await Task.WhenAll(task1, task2);
            }
            catch (Exception e)
            {
                Frame.Navigate(typeof(Error), new Error.Options
                {
                    Exception = e,
                    Message = "Failed to load media from the server.",
                    Page = GetType()
                });
            }
            
        }

        /// <summary>
        /// Populates the "Up Next" list on the Dashboard
        /// </summary>
        private async Task LoadUpNext()
        {
            UpNextCards.IsBusy = true;

            var upNext = await _app.State.DataSource.GetUpNextMediaItems();

            // TODO: Merge instead of replace
            _upNext.Clear();

            UpNextCards.IsBusy = false;

            if (upNext.Count == 0)
            {
                UpNextCards.ShowTextMessage("Nothing here? Why not start watching something?");
            }

            foreach (var item in upNext)
            {
                _upNext.Add(item);
            }
        }

        /// <summary>
        /// Populates the recently added movie/episode lists
        /// </summary>
        private async Task LoadRecentlyAdded()
        {
            RecentMovieCards.IsBusy = true;
            RecentEpisodeCards.IsBusy = true;

            var recentlyAdded = await _app.State.DataSource.GetRecentlyAddedMediaItems();

            // TODO: Merge instead of replace
            _recentlyAddedMovies.Clear();
            _recentlyAddedEpisodes.Clear();

            RecentMovieCards.IsBusy = false;
            RecentEpisodeCards.IsBusy = false;

            foreach (var item in recentlyAdded)
            {
                if (item is Movie) _recentlyAddedMovies.Add(item);
                if (item is Episode) _recentlyAddedEpisodes.Add(item);
            }

            if (_recentlyAddedEpisodes.Count == 0)
            {
                RecentEpisodeCards.ShowTextMessage("You currently have no series.");
            }
            if (_recentlyAddedMovies.Count == 0)
            {
                RecentMovieCards.ShowTextMessage("You currently have no movies.");
            }
        }

        private void MediaItemList_MovieClicked(object _, Movie movie)
        {
            Frame.Navigate(typeof(MovieDetails), movie.Uuid);
        }

        private void MediaItemList_EpisodeClicked(object _, Episode episode)
        {
            Frame.Navigate(typeof(EpisodeDetails), episode.Uuid);
        }
    }
}