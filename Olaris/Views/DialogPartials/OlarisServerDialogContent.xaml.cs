﻿using Olaris.Models;
using System;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Olaris.Views.DialogPartials
{
    public sealed partial class OlarisServerDialogContent : UserControl
    {
        private App _app;
        private OlarisServer _server;

        /// <summary>
        /// Event that fires when the server is saved. Returns the saved server
        /// </summary>
        public event EventHandler<OlarisServer> Saved;

        /// <summary>
        /// Event that fires when the dialog is cancelled
        /// </summary>
        public event EventHandler Cancelled;

        /// <summary>
        /// Constructor with no arguments.
        /// </summary>
        public OlarisServerDialogContent()
        {
            _app = (App)Application.Current;
            _server = new OlarisServer();
            InitializeComponent();
        }

        /// <summary>
        /// Constructor that takes an OlarisServer that will be populated for
        /// editing.
        /// </summary>
        /// <param name="server"></param>
        public OlarisServerDialogContent(OlarisServer server)
        {
            _app = (App)Application.Current;
            _server = server;
            InitializeComponent();
            _ = LoadFields();
        }

        /// <summary>
        /// Load the OlarisServer data into the form fields.
        /// </summary>
        /// <returns></returns>
        private async Task LoadFields()
        {
            ServerName.Text = _server.Name ?? "";
            ServerAddress.Text = _server.BuildBaseUrl();
            ServerUserName.Text = _server.UserName;
            ServerPassword.Password = await _server.GetPassword();
        }

        /// <summary>
        /// Returns the first missing field name it encouters.
        /// </summary>
        /// <returns></returns>
        private string MissingFieldName()
        {
            if (string.IsNullOrEmpty(ServerAddress.Text))
            {
                return "server address";
            }

            if (string.IsNullOrEmpty(ServerUserName.Text))
            {
                return "username";
            }

            if (string.IsNullOrEmpty(ServerPassword.Password))
            {
                return "password";
            }

            return null;
        }

        /// <summary>
        /// Save the server to the local cache.
        /// </summary>
        /// <returns></returns>
        private async Task<bool> SaveFields()
        {
            NotificationBox.Dismiss();

            // Check for missing fields
            var missingField = MissingFieldName();
            if (missingField != null)
            {
                NotificationBox.Show($"{missingField} is required");
                return false;
            }

            // Make sure the address is valid and HTTP or HTTPS
            Uri uriResult;
            if (!Uri.TryCreate(ServerAddress.Text, UriKind.Absolute, out uriResult) || (uriResult.Scheme != Uri.UriSchemeHttp && uriResult.Scheme != Uri.UriSchemeHttps))
            {
                NotificationBox.Show("server address is not valid");
                return false;
            }

            _server.ParseUrl(ServerAddress.Text);
            _server.Name = ServerName.Text;
            _server.UserName = ServerUserName.Text;
            await _server.SetPassword(ServerPassword.Password);

            _app.State.LocalCache.SaveOlarisServer(_server);
            return true;
        }

        /// <summary>
        /// Fire the Saved event and pass it the _server value.
        /// </summary>
        private async void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            var saved = await SaveFields();
            if (saved == true)
            {
                Saved?.Invoke(this, _server);
            }
        }

        /// <summary>
        /// Fire the Cancelled event.
        /// </summary>
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Cancelled?.Invoke(this, new EventArgs());
        }
    }
}
