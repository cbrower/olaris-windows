﻿using Olaris.Models;
using System;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace Olaris.Views
{
    /// <summary>
    /// View that shows a episode's information
    /// </summary>
    public sealed partial class EpisodeDetails : Page, IRefreshable, INotifyPropertyChanged
    {
        private App _app;
        public Episode Episode { get; set; }
        public string Still { get; set; } = (string)Application.Current.Resources["DefaultStillPath"];
        public double Progress { get; set; } = 0;

        private string _previousEpisodeUuid;
        private string _nextEpisodeUuid;

        /// <summary>
        /// Required for INotifyPropertyChanged
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Constructor
        /// </summary>
        public EpisodeDetails()
        {
            _app = ((App)Application.Current);
            InitializeComponent();
        }

        /// <summary>
        /// When navigated to, get the UUID of the Episode and fetch the details from the server.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            var uuid = (string)e.Parameter;
            _ = GetEpisodeDetails(uuid);
            _ = GetNearbyEpisodes(uuid);
        }

        /// <summary>
        /// Return a string in the "Episode 1" format
        /// </summary>
        private string EpisodeNumberString
        {
            get => Episode == null ? "" : String.Format("Episode {0}", Episode.EpisodeNumber);
        }

        /// <summary>
        /// Return a string in the "Season 1" format
        /// </summary>
        private string SeasonNumberString
        {
            get => Episode == null ? "" : String.Format("Season {0}", Episode.Season.SeasonNumber);
        }

        /// <summary>
        /// Returns either "Play" or "Resume" depending on the PlayState
        /// </summary>
        private string playButtonText
        {
            get
            {
                if (Episode != null && Episode.PlayState.CanResume)
                {
                    return "Resume";
                }
                return "Play";
            }
        }

        /// <summary>
        /// Gets the episode's details from the server
        /// </summary>
        /// <param name="uuid"></param>
        private async Task GetEpisodeDetails(string uuid)
        {
            try
            {
                Episode = await _app.State.DataSource.GetEpisodeById(uuid);
            }
            catch (Exception ex)
            {
                Frame.Navigate(typeof(Error), new Error.Options
                {
                    Exception = ex,
                    Message = "Error loading episode information.",
                    Page = GetType()
                });
                return;
            }

            if (!string.IsNullOrEmpty(Episode.StillPath))
            {
                Still = Episode.StillPath;
            }
            
            Progress = Episode.PlayState.PlayTime / Episode.Files.First().TotalDuration;
            _app.RequestBackdropFromUri(Episode.Season.Series.BackdropPath);
            Bindings.Update();
            MediaFileSelector.SelectedIndex = 0;
        }

        /// <summary>
        /// Load the episode on either side of this one so the user can navigate
        /// to the previous/next from this page.
        /// </summary>
        private async Task GetNearbyEpisodes(string episodeUuid)
        {
            try
            {
                var nearbyEpisodes = await _app.State.DataSource.GetNearbyEpisodes(episodeUuid, 1);
                _previousEpisodeUuid = nearbyEpisodes.Previous.FirstOrDefault()?.Uuid;
                _nextEpisodeUuid = nearbyEpisodes.Next.FirstOrDefault()?.Uuid;
            }
            catch(Exception ex)
            {
                Frame.Navigate(typeof(Error), new Error.Options
                {
                    Exception = ex,
                    Message = "Error loading nearby episodes",
                    Page = GetType()
                });
                return;
            }

            NotifyPropertyChanged(nameof(_previousEpisodeUuid));
            NotifyPropertyChanged(nameof(_nextEpisodeUuid));
        }

        /// <summary>
        /// When the file selected is changed, update the file metadata fields
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var c = sender as ComboBox;
            var episodefile = (EpisodeFile)c.SelectedItem;

            if (episodefile == null)
            {
                _mediaFileInfo.Visibility = Visibility.Collapsed;
            }
            else
            {
                SubtitleList.Text = string.Join(" ", episodefile.SubtitleList());
                AudioTrackList.Text = string.Join(" ", episodefile.AudioTrackList());
                Resolution.Text = episodefile.Streams.First(x => x.StreamType == "video").Resolution;
                Runtime.Text = Math.Truncate(episodefile.TotalDuration / 60) + " minutes";
                _mediaFileInfo.Visibility = Visibility.Visible;
            }
        }

        /// <summary>
        /// Handle the click event for the play button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlayEpisode(object sender, RoutedEventArgs e)
        {
            if (Episode == null)
            {
                return;
            }

            var selectedFile = (EpisodeFile)MediaFileSelector.SelectedItem;
            if (Episode.PlayState.CanResume == false)
            {
                _app.GoToPlayer(new Player.Options { MediaFileUuid = selectedFile.Uuid, MediaItemUuid = Episode.Uuid, StartAt = new TimeSpan(0, 0, 0) });
                return;
            }

            _app.GoToPlayer(new Player.Options { MediaFileUuid = selectedFile.Uuid, MediaItemUuid = Episode.Uuid, StartAt = Episode.PlayState.AsTimeSpan() });
        }

        /// <summary>
        /// Returns the visibility that should be applied to the "Mark Watched" button
        /// </summary>
        public Visibility MarkWatchedVisibility
        {
            get
            {
                if (Episode == null)
                {
                    return Visibility.Collapsed;
                }
                return Episode.PlayState.Finished ? Visibility.Collapsed : Visibility.Visible;
            }
        }

        /// <summary>
        /// Returns the visibility that should be applied to the "Mark Unwatched" button
        /// </summary>
        public Visibility MarkUnwatchedVisibility
        {
            get
            {
                if (Episode == null)
                {
                    return Visibility.Collapsed;
                }
                return Episode.PlayState.Finished ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Handles the click event on the "Mark Watched" button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void MarkWatched_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                NotificationBox.Dismiss();
                _ = await _app.State.DataSource.CreatePlayState(Episode.Uuid, true, 0);
            }
            catch(Exception ex)
            {
                NotificationBox.Show(ex.Message);
                return;
            }

            _ = GetEpisodeDetails(Episode.Uuid);
            Bindings.Update();
        }

        /// <summary>
        /// Handles the click event on the "Mark Unwatched" button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void MarkUnwatched_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                NotificationBox.Dismiss();
                _ = await _app.State.DataSource.CreatePlayState(Episode.Uuid, false, 0);
            }
            catch (Exception ex)
            {
                NotificationBox.Show(ex.Message);
                return;
            }
            
            _ = GetEpisodeDetails(Episode.Uuid);
            Bindings.Update();
        }

        /// <summary>
        /// Navigate to this episode's season.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void SeasonLink_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(SeasonView), Episode.Season.Uuid);

        }

        /// <summary>
        /// Navigate to this episode's series.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void SeriesLink_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(SeriesView), Episode.Season.Series.Uuid);

        }

        /// <summary>
        /// Navigate to the previous episode.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PreviousEpisode_Click(object sender, RoutedEventArgs e)
        {
            if (_previousEpisodeUuid == null)
            {
                return;
            }
            Frame.Navigate(typeof(EpisodeDetails), _previousEpisodeUuid);
        }

        /// <summary>
        /// Navigate to the next episode.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NextEpisode_Click(object sender, RoutedEventArgs e)
        {
            if (_nextEpisodeUuid == null)
            {
                return;
            }
            Frame.Navigate(typeof(EpisodeDetails), _nextEpisodeUuid);
        }

        #region Implementation: IRefreshable

        /// <summary>
        /// Refresh the data for the current episode, if one exists.
        /// </summary>
        public void Refresh()
        {
            if (Episode != null)
            {
                _ = GetEpisodeDetails(Episode.Uuid);
            }
        }

        #endregion

        /// <summary>
        /// Required for INotifyPropertyChanged
        /// </summary>
        /// <param name="propertyName">
        /// Name of the parameter that changed.
        /// </param>
        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
