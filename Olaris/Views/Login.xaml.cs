﻿using Olaris.Data;
using Olaris.Models;
using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Olaris.Views
{
    /// <summary>
    /// The dashboard that shows next episodes and recently added content
    /// </summary>
    public sealed partial class Login : Page
    {
        private App _app;

        public Login()
        {
            _app = ((App)Application.Current);
            InitializeComponent();
        }

        private void ShowLoadingAnimation()
        {
            LoadingIndicator.IsActive = true;
            LoginText.Visibility = Visibility.Collapsed;
        }

        private void HideLoadingAnimation()
        {
            LoginText.Visibility = Visibility.Visible;
            LoadingIndicator.IsActive = false;
        }

        // Test the credentials given, and if they work, save them in the app
        // state and continue to the main page.
        private async void Continue_OnClick(object sender, RoutedEventArgs e)
        {
            ShowLoadingAnimation();
            NotificationBox.Dismiss();

            string serverAddress = ServerAddress.Text;
            string username = UserId.Text;
            string password = Password.Password;
            string serverName = ServerName.Text;

            OlarisServer testServer;

            // Make sure the URL is valid and either HTTP or HTTPS
            Uri uriResult;
            if (!Uri.TryCreate(serverAddress, UriKind.Absolute, out uriResult) || (uriResult.Scheme != Uri.UriSchemeHttp && uriResult.Scheme != Uri.UriSchemeHttps))
            {
                NotificationBox.Show("Invalid server address");
                HideLoadingAnimation();
                return;
            }

            // This will attempt to turn the server address into a URL object,
            // which could throw an exception if it isn't a valid URL.
            try
            {
                testServer = new OlarisServer(uriResult.AbsoluteUri, username, serverName);
            }
            catch (Exception ex)
            {
                NotificationBox.Show(string.Format("Error: {0}", ex.Message));
                HideLoadingAnimation();
                return;
            }

            // Try authenticating with the provided credentials
            try
            {
                var authWorks = await testServer.TestCredentials(password);
                if (!authWorks)
                {
                    NotificationBox.Show("Authentication failed");
                    HideLoadingAnimation();
                    return;
                }
            }
            catch(Exception ex)
            {
                NotificationBox.Show(string.Format("Error: {0}", ex.Message));
                HideLoadingAnimation();
                return;
            }

            await testServer.SetPassword(password);
            _app.State.LocalCache.SaveOlarisServer(testServer);
            _app.State.LocalCache.SetDefaultOlarisServer(testServer.OlarisServerId);

            _app.State.DataSource = new GraphQLSource(testServer);
            _app.GoToMainPage();
        }
    }
}