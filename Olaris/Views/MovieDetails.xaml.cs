﻿using Olaris.Models;
using System;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace Olaris.Views
{
    /// <summary>
    /// View that shows a movie's information
    /// </summary>
    public sealed partial class MovieDetails : Page, IRefreshable
    {
        private App _app;
        public Movie Movie { get; set; }
        public string Poster { get; set; } = (string)Application.Current.Resources["DefaultPosterPath"];
        public double Progress { get; set; } = 0;

        /// <summary>
        /// Constructor
        /// </summary>
        public MovieDetails()
        {
            _app = ((App)Application.Current);
            InitializeComponent();
        }

        /// <summary>
        /// When navigated to, get the UUID of the Movie and fetch the details from the server.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            var uuid = (string)e.Parameter;
            GetMovieDetails(uuid);
        }

        /// <summary>
        /// Gets the movie's details from the server
        /// </summary>
        /// <param name="uuid"></param>
        private async void GetMovieDetails(string uuid)
        {
            try
            {
                Movie = await _app.State.DataSource.GetMovieById(uuid);
            }
            catch (Exception ex)
            {
                Frame.Navigate(typeof(Error), new Error.Options
                {
                    Exception = ex,
                    Message = "Error loading movie information.",
                    Page = GetType()
                });
                return;
            }

            if (!string.IsNullOrEmpty(Movie.PosterPath))
            {
                Poster = Movie.PosterPath;
            }
            
            Progress = Movie.PlayState.PlayTime / Movie.Files.First().TotalDuration;
            Bindings.Update();
            MovieFileSelector.SelectedIndex = 0;
            _app.RequestBackdropFromUri(Movie.BackdropPath);
        }

        /// <summary>
        /// When the file selected is changed, update the file metadata fields
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var c = sender as ComboBox;
            var moviefile = (MovieFile)c.SelectedItem;

            if (moviefile == null)
            {
                _mediaFileInfo.Visibility = Visibility.Collapsed;
            }
            else
            {
                SubtitleList.Text = string.Join(" ", moviefile.SubtitleList());
                AudioTrackList.Text = string.Join(" ", moviefile.AudioTrackList());
                Resolution.Text = moviefile.Streams.First(x => x.StreamType == "video").Resolution;
                Runtime.Text = Math.Truncate(moviefile.TotalDuration / 60) + " minutes";
                _mediaFileInfo.Visibility = Visibility.Visible;
            }
        }

        /// <summary>
        /// Handle the click event for the play button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlayMovie(object sender, RoutedEventArgs e)
        {
            if (Movie == null)
            {
                return;
            }

            var selectedFile = (MovieFile)MovieFileSelector.SelectedItem;
            if (Movie.PlayState.CanResume == false)
            {
                _app.GoToPlayer(new Player.Options { MediaFileUuid = selectedFile.Uuid, MediaItemUuid = Movie.Uuid, StartAt = new TimeSpan(0, 0, 0) });
                return;
            }

            _app.GoToPlayer(new Player.Options { MediaFileUuid = selectedFile.Uuid, MediaItemUuid = Movie.Uuid, StartAt = Movie.PlayState.AsTimeSpan() });
        }

        /// <summary>
        /// Returns the visibility that should be applied to the "Mark Watched" button
        /// </summary>
        public Visibility MarkWatchedVisibility
        {
            get
            {
                if (Movie == null)
                {
                    return Visibility.Collapsed;
                }
                return Movie.PlayState.Finished ? Visibility.Collapsed : Visibility.Visible;
            }
        }

        /// <summary>
        /// Returns the visibility that should be applied to the "Mark Unwatched" button
        /// </summary>
        public Visibility MarkUnwatchedVisibility
        {
            get
            {
                if (Movie == null)
                {
                    return Visibility.Collapsed;
                }
                return Movie.PlayState.Finished ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Returns either "Play" or "Resume" depending on the PlayState
        /// </summary>
        private string playButtonText
        {
            get
            {
                if (Movie != null && Movie.PlayState.CanResume)
                {
                    return "Resume";
                }
                return "Play";
            }
        }

        /// <summary>
        /// Handles the click event on the "Mark Watched" button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void MarkWatched_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                NotificationBox.Dismiss();
                _ = await _app.State.DataSource.CreatePlayState(Movie.Uuid, true, 0);
            }
            catch(Exception ex)
            {
                NotificationBox.Show(ex.Message);
                return;
            }

            GetMovieDetails(Movie.Uuid);
            Bindings.Update();
        }

        /// <summary>
        /// Handles the click event on the "Mark Unwatched" button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void MarkUnwatched_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                NotificationBox.Dismiss();
                _ = await _app.State.DataSource.CreatePlayState(Movie.Uuid, false, 0);
            }
            catch (Exception ex)
            {
                NotificationBox.Show(ex.Message);
                return;
            }
            
            GetMovieDetails(Movie.Uuid);
            Bindings.Update();
        }

        #region Impleementation: IRefreshable

        /// <summary>
        /// If there is a movie, refresh its data from the server.
        /// </summary>
        public void Refresh()
        {
            if (Movie != null)
            {
                GetMovieDetails(Movie.Uuid);
            }
        }

        #endregion
    }
}
