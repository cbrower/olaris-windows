﻿using Olaris.Data;
using Olaris.Models;
using Olaris.Views.DialogPartials;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Navigation;

namespace Olaris.Views
{
    /// <summary>
    /// The settings page.
    /// </summary>
    public sealed partial class SettingsView : Page, INotifyPropertyChanged
    {
        private App _app;
        private ObservableCollection<OlarisServer> _olarisServers;
        private ContentDialog _contentDialog;
        private OlarisServerDialogContent _olarisServerDialogContent;
        private bool _dialogOpen;

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Constructor
        /// </summary>
        public SettingsView()
        {
            _app = (App)Application.Current;
            _olarisServers = new ObservableCollection<OlarisServer>();
            _contentDialog = new ContentDialog();

            InitializeComponent();
            GetServers();

            BackgroundImageBlurValue.Value = _app.State.LocalCache.ApplicationSettings.BackdropImageBlurAmount * 4;
        }

        /// <summary>
        /// Notify INotifyPropertyChanged listeners that a property has changed.
        /// </summary>
        /// <param name="propertyName"></param>
        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// On navigated to...
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            _app.ClearBackdrop();
        }

        /// <summary>
        /// Gets the Olaris server configurations from the local cache and adds
        /// them to the _olarisServers.
        /// </summary>
        private void GetServers()
        {
            _olarisServers.Clear();

            foreach (var item in _app.State.LocalCache.GetOlarisServers())
            {
                _olarisServers.Add(item);
            }
        }

        /// <summary>
        /// When the server is changed, it's necessary to clear any pages that
        /// have been cached so the user doesn't see content from the old
        /// server.
        /// </summary>
        private void ResetCache()
        {
            if (Parent is Frame parentFrame)
            {
                // Clear the Frame's page cache
                var cacheSize = parentFrame.CacheSize;
                parentFrame.CacheSize = 0;
                parentFrame.CacheSize = cacheSize;

                // Clear the navigation history - prevents navigation to items
                // that don't exist in the current olaris server.
                parentFrame.BackStack.Clear();
            }
        }

        /// <summary>
        /// Removes an OlarisServer configuration from the app.
        /// </summary>
        private void DeleteServerConfiguration(OlarisServer server)
        {
            var defaultServer = _app.State.LocalCache.GetDefaultOlarisServer();

            _app.State.LocalCache.RemoveOlarisServer(server.OlarisServerId);

            // We deleted the default server!
            if (server.OlarisServerId == defaultServer.OlarisServerId)
            {
                if (!_app.State.LocalCache.ChooseNewDefaultServer())
                {
                    _app.GoToLogin();
                    return;
                }
            }

            // We deleted the active server!
            if (_app.State.DataSource.ServerId == server.OlarisServerId)
            {
                ActivateServer(_app.State.LocalCache.GetDefaultOlarisServer());
            }

            GetServers();
        }

        /// <summary>
        /// Opens the dialog to add a new Olaris server configuration.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddServerButton_Click(object sender, RoutedEventArgs e)
        {
            _olarisServerDialogContent = new OlarisServerDialogContent();
            ShowOlarisServerDialog();
        }

        /// <summary>
        /// Registers the event handlers and shows the content dialog.
        /// </summary>
        private async void ShowOlarisServerDialog()
        {
            if (_dialogOpen)
            {
                return;
            }
            _dialogOpen = true;

            _contentDialog.Content = _olarisServerDialogContent;

            // Register event handlers
            _olarisServerDialogContent.Saved += ServerDialog_Saved;
            _olarisServerDialogContent.Cancelled += ServerDialog_Cancelled;
            _contentDialog.Closed += DisposeServerDialogContent;

            await _contentDialog.ShowAsync();
        }

        /// <summary>
        /// Changes the active Olaris server for the app.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConnectButton_Click(object sender, OlarisServer server)
        {
            _app.State.LocalCache.SetDefaultOlarisServer(server.OlarisServerId);
            ActivateServer(server);
            GetServers();
        }

        /// <summary>
        /// Activates an Olaris server.
        /// </summary>
        /// <param name="server"></param>
        private void ActivateServer(OlarisServer server)
        {
            _app.State.DataSource = new GraphQLSource(server);
            ResetCache();
            _app.NotifyServerChanged(server);
        }

        /// <summary>
        /// Removes the Olaris server from the app.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteButton_Click(object sender, OlarisServer server)
        {
            DeleteServerConfiguration(server);
        }

        /// <summary>
        /// Open the OlarisServer configuration.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditButton_Click(object sender, OlarisServer server)
        {
            _olarisServerDialogContent = new OlarisServerDialogContent(server);
            ShowOlarisServerDialog();
        }

        /// <summary>
        /// Saves the blur value when the slider changes. Dividing by 4 lets us
        /// offer a 0-100 scale for values 0-25. It's a double type, so
        /// we can make use of values that don't evenly divide.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BackgroundImageBlurValue_ValueChanged(object _, RangeBaseValueChangedEventArgs e)
        {
            _app.State.LocalCache.SaveBackdropImageBlurAmountPreference(e.NewValue / 4);
        }

        /// <summary>
        /// If cancelled, just close the dialog.
        /// </summary>
        private void ServerDialog_Cancelled(object sender, EventArgs _)
        {
            _contentDialog.Hide();
        }

        /// <summary>
        /// If saved, reload the server list, and if the current active server  
        /// was changed, re-initialize the app data source to apply.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="server"></param>
        private void ServerDialog_Saved(object sender, OlarisServer server)
        {
            _contentDialog.Hide();

            GetServers();

            if (_app.State.DataSource.ServerId == server.OlarisServerId)
            {
                ActivateServer(server);
            }
        }

        /// <summary>
        /// Un-register event handlers to prevent a memory leak, and null the
        /// controls.
        /// </summary>
        private void DisposeServerDialogContent(ContentDialog d, ContentDialogClosedEventArgs a)
        {
            _olarisServerDialogContent.Saved -= ServerDialog_Saved;
            _olarisServerDialogContent.Cancelled -= ServerDialog_Cancelled;

            _contentDialog.Closed -= DisposeServerDialogContent;
            _olarisServerDialogContent = null;
            _dialogOpen = false;
        }

        /// <summary>
        /// Go to the logs page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LogsButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Logs));
        }
    }
}
