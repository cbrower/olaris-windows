﻿namespace Olaris.Views
{
    /// <summary>
    /// Indicates that the item supports refreshing. Used to tell the layouts if
    /// their framed pages want to be refreshed after loading.
    /// </summary>
    public interface IRefreshable
    {
        void Refresh();
    }
}
