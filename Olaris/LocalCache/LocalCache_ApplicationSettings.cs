﻿using Olaris.Models;
using System;
using System.Linq;

namespace Olaris
{
    public partial class LocalCache
    {
        /// <summary>
        /// Event is fired when the application settings are modified.
        /// </summary>
        public event EventHandler<ApplicationSettings> ApplicationSettingsChanged;

        public ApplicationSettings ApplicationSettings
        {
            get => _applicationSettings;
        }

        // Returns the application settings object. If one does not exist, it creates a new one and saves it to the DB.
        private ApplicationSettings RetrieveOrCreateApplicationSettings()
        {
            var collection = SettingCollection;
            var documentCount = collection.Count();
            ApplicationSettings settings;

            switch (documentCount)
            {
                case 0:
                    settings = CreateApplicationSettingsIfNotExists();
                    break;
                case 1:
                    settings = collection.FindAll().First();
                    break;
                default:
                    throw new Exception("found more than one settings object");
            }

            return settings;
        }

        // Create a new ApplicationSettings object and save it to the database. If someone beat us to it, fetch and
        // return the existing value.
        private ApplicationSettings CreateApplicationSettingsIfNotExists()
        {
            _settingsMutex.WaitOne();

            var collection = SettingCollection;
            var documentCount = collection.Count();
            ApplicationSettings settings;

            if (documentCount < 1)
            {
                settings = new ApplicationSettings();
                collection.Insert(settings);
            }
            else
            {
                settings = collection.FindAll().GetEnumerator().Current;
            }

            _settingsMutex.ReleaseMutex();
            return settings;
        }

        /// <summary>
        /// Save the current application settings to the database.
        /// </summary>
        /// <remarks>
        /// TODO: This seems to fail if any new properties are added.
        /// </remarks>
        private void SaveApplicationSettings()
        {
            var collection = SettingCollection;
            collection.Update(_applicationSettings);
            ApplicationSettingsChanged?.Invoke(this, _applicationSettings);
        }

        /// <summary>
        /// Save the user's volume preference to the cache database so it can be persisted.
        /// </summary>
        /// <param name="volume"></param>
        public void SaveVolumePreference(double volume)
        {
            _applicationSettings.Volume = volume;
            SaveApplicationSettings();
            
        }

        /// <summary>
        /// Saves the user's backdrop blur amount preference.
        /// </summary>
        /// <param name="amount"></param>
        public void SaveBackdropImageBlurAmountPreference(double amount)
        {
            _applicationSettings.BackdropImageBlurAmount = amount;
            SaveApplicationSettings();
        }
    }
}
