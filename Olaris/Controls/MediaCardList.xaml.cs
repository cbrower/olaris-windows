﻿using Olaris.Models;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Olaris.Controls
{
    public sealed partial class MediaCardList : UserControl, INotifyPropertyChanged
    {
        #region Fields

        private IList<IMediaItem> _listSource;
        private bool _isBusy = false;
        private string _title;

        #endregion

        #region Delegates

        public delegate void MovieClickedEventHandler(object sender, Movie movie);

        public delegate void EpisodeClickedEventHandler(object sender, Episode episode);

        #endregion

        #region Constructors

        public MediaCardList()
        {
            InitializeComponent();
        }

        #endregion

        #region Events

        public event MovieClickedEventHandler MovieClicked;

        public event EpisodeClickedEventHandler EpisodeClicked;

        #endregion

        #region Properties

        public IList<IMediaItem> ListSource
        {
            get => _listSource;
            set
            {
                _listSource = value;
                NotifyPropertyChanged("ListSource");
            }
        }

        public bool IsBusy
        {
            get => _isBusy;
            set
            {
                _isBusy = value;
                NotifyPropertyChanged(nameof(IsBusy));
            }
        }

        public string Title
        {
            get => _title;
            set
            {
                _title = value;
                NotifyPropertyChanged("Title");
            }
        }

        public Visibility PreviousVisibility
        {
            get
            {
                if (ItemScrollView.HorizontalOffset == 0)
                {
                    return Visibility.Collapsed;
                }
                return Visibility.Visible;
            }
        }

        public Visibility NextVisibility
        {
            get
            {
                if (ItemScrollView.HorizontalOffset == ItemScrollView.ScrollableWidth)
                {
                    return Visibility.Collapsed;
                }
                return Visibility.Visible;
            }
        }

        #endregion

        #region Methods

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void NotifyMovieClicked(Movie movie)
        {
            MovieClicked?.Invoke(this, movie);
        }

        private void NotifyEpisodeClicked(Episode episode)
        {
            EpisodeClicked?.Invoke(this, episode);
        }

        public void ShowTextMessage(string message)
        {
            TextMessage.Text = message;
            TextMessageContainer.Visibility = Visibility.Visible;
        }

        public void HideTextMessage()
        {
            TextMessageContainer.Visibility = Visibility.Collapsed;
        }

        #endregion

        #region EventHandlers

        /// <summary>
        /// Handles click events on the list and fires a more specific event based on the type of media item.
        /// </summary>
        private void ListView_ItemClick(object _, ItemClickEventArgs e)
        {
            var selectedItem = e.ClickedItem;

            if (selectedItem is Movie)
            {
                NotifyMovieClicked(selectedItem as Movie);
            }

            if (selectedItem is Episode)
            {
                NotifyEpisodeClicked(selectedItem as Episode);
            }
        }

        private void ItemScrollView_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            NotifyPropertyChanged("PreviousVisibility");
            NotifyPropertyChanged("NextVisibility");
        }

        private void ItemScrollView_ViewChanged(object sender, ScrollViewerViewChangedEventArgs e)
        {
            NotifyPropertyChanged("PreviousVisibility");
            NotifyPropertyChanged("NextVisibility");
        }

        private void Previous_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            ItemScrollView.ChangeView(ItemScrollView.HorizontalOffset - ItemScrollView.ViewportWidth, null, null, false);
        }

        private void Next_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            ItemScrollView.ChangeView(ItemScrollView.HorizontalOffset + ItemScrollView.ViewportWidth, null, null, false);
        }

        #endregion

        #region Implementation: INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
