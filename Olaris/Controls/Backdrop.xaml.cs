﻿using System;
using System.ComponentModel;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using Microsoft.Toolkit.Uwp.UI.Animations;
using System.Runtime.CompilerServices;

namespace Olaris.Controls
{
    /// <summary>
    /// Backdrop is meant to be the app's background image.
    /// </summary>
    public sealed partial class Backdrop : UserControl, INotifyPropertyChanged
    {
        private BitmapImage _imageSource = new BitmapImage();
        private double _animationDuration = 1;
        private double _targetOpacity = 0.075;
        private double _blurAmount;

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Constructor
        /// </summary>
        public Backdrop()
        {
            InitializeComponent();
        }

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public double BlurAmount
        {
            get { return _blurAmount; }
            set { _blurAmount = value; NotifyPropertyChanged(nameof(BlurAmount)); }
        }

        /// <summary>
        /// The image that is displayed.
        /// </summary>
        public BitmapImage ImageSource
        {
            get => _imageSource;
            set
            {
                SetBackdrop(value);
            }
        }

        /// <summary>
        /// Updates the image source and runs the correct animations.
        /// </summary>
        /// <remarks>
        /// There is probably a better way to do this.
        /// </remarks>
        /// <param name="source"></param>
        private async void SetBackdrop(BitmapImage source)
        {
            if (_imageSource.UriSource == null && source == null) return;

            if (_imageSource.UriSource != null && source == null)
            {
                await AnimationBuilder.Create().Opacity(from: _targetOpacity, to: 0, duration: TimeSpan.FromSeconds(_animationDuration)).StartAsync(BackdropImage);
                _imageSource.UriSource = null;
                return;
            }

            if (_imageSource.UriSource == source.UriSource) return;

            await AnimationBuilder.Create().Opacity(from: _targetOpacity, to: 0, duration: TimeSpan.FromSeconds(_animationDuration)).StartAsync(BackdropImage);
            _imageSource.UriSource = source.UriSource;
        }

        /// <summary>
        /// Fires when the image finishes loading, and fades it in.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BackdropImage_Loaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            AnimationBuilder.Create().Opacity(from: 0, to: _targetOpacity, duration: TimeSpan.FromSeconds(_animationDuration)).StartAsync(BackdropImage);
        }
    }
}
