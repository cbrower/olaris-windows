﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Olaris.Controls
{
    public sealed class PlayerControls : MediaTransportControls
    {
        public event EventHandler<EventArgs> Exited;
        public event EventHandler<EventArgs> SkipForwardsRequested;
        public event EventHandler<EventArgs> SkipBackwardsRequested;

        public PlayerControls()
        {
            DefaultStyleKey = typeof(PlayerControls);
        }

        protected override void OnApplyTemplate()
        {
            // Find the custom button and create an event handler for its Click event.
            var exitButton = GetTemplateChild("ExitButton") as Button;
            exitButton.Click += ExitButton_Click;

            var skipForwardsButton = GetTemplateChild("SkipForwardsButton") as Button;
            skipForwardsButton.Click += SkipForwardsButton_Click;

            var skipBackwardsButton = GetTemplateChild("SkipBackwardsButton") as Button;
            skipBackwardsButton.Click += SkipBackwardsButton_Click;

            base.OnApplyTemplate();
        }

        /// <summary>
        /// Raise an event on the custom control when 'Exit' is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            Exited?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// Raise an event on the custom control when 'Skip Forwards' is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SkipForwardsButton_Click(object sender, RoutedEventArgs e)
        {
            SkipForwardsRequested?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// Raise an event on the custom control when 'Skip Backwards' is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SkipBackwardsButton_Click(object sender, RoutedEventArgs e)
        {
            SkipBackwardsRequested?.Invoke(this, EventArgs.Empty);
        }
    }
}
