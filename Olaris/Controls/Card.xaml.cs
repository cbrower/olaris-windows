﻿using Olaris.Models;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.UI.Xaml.Controls;

namespace Olaris.Controls
{
    public sealed partial class Card : UserControl, INotifyPropertyChanged
    {
        #region Fields

        private double _width;
        private ICardItem _cardItem;

        #endregion

        #region Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Constructors

        public Card()
        {
            InitializeComponent();
        }

        #endregion

        #region Properties

        public ICardItem CardItem
        {
            get => _cardItem;
            set
            {
                _cardItem = value;
                NotifyPropertyChanged("CardItem");
            }
        }

        public new double Width
        {
            get => _width;
            set
            {
                _width = value;
                NotifyPropertyChanged("Width");
                NotifyPropertyChanged("Height");
            }
        }

        public new double Height
        {
            get => _width * 1.5;
        }

        #endregion

        #region Methods

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
