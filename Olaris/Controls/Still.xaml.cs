﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Olaris.Controls
{
    public sealed partial class Still : UserControl, INotifyPropertyChanged
    {
        #region Fields

        private string _stillPath;
        private bool _watched;
        private double _progress;
        private int _count;
        private int _minCount;

        #endregion

        #region Constructors

        public Still()
        {
            InitializeComponent();
        }

        #endregion

        #region Properties

        // Full URL path to the item's still image
        public string StillPath
        {
            get => string.IsNullOrEmpty(_stillPath) ? (string)Application.Current.Resources["DefaultStillPath"] : _stillPath;
            set
            {
                _stillPath = value;
                NotifyPropertyChanged("StillPath");
            }
        }

        public bool Watched
        {
            get => _watched;
            set
            {
                _watched = value;
                NotifyPropertyChanged("Watched");
            }
        }

        // 0 = 0%, 1 = 100%
        public double Progress
        {
            get => _progress;
            set
            {
                _progress = value;
                NotifyPropertyChanged("Progress");
                NotifyPropertyChanged("ProgressBarVisibility");
            }
        }

        public int Count
        {
            get => _count;
            set
            {
                _count = value;
                NotifyPropertyChanged("Count");
                NotifyPropertyChanged("CountVisibility");
            }
        }

        public int MinCount
        {
            get => _minCount;
            set
            {
                _minCount = value;
                NotifyPropertyChanged("MinCount");
                NotifyPropertyChanged("CountVisibility");
            }
        }

        // Only make the progress bar visible if the item is partially watched.
        public Visibility ProgressBarVisibility
        {
            get
            {
                if (_progress > 0 && !_watched) { return Visibility.Visible; }
                else { return Visibility.Collapsed; }
            }
        }

        /// <summary>
        /// Hide the count if it's less than the minimum allowed count value.
        /// </summary>
        public Visibility CountVisibility
        {
            get
            {
                return _count >= _minCount ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        #endregion

        #region Methods

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region Implementation: INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
