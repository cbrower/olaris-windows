﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Olaris.Controls
{
    public sealed partial class Poster : UserControl, INotifyPropertyChanged
    {
        private string _posterPath;
        private bool _watched;
        private double _progress;
        private int _count;
        private int _minCount;

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        // Full URL path to the item's poster image
        public string PosterPath
        {
            get => string.IsNullOrEmpty(_posterPath) ? (string)Application.Current.Resources["DefaultPosterPath"] : _posterPath;
            set
            {
                _posterPath = value;
                NotifyPropertyChanged("PosterPath");
            }
        }

        public bool Watched
        {
            get => _watched;
            set
            {
                _watched = value;
                NotifyPropertyChanged("Watched");
            }
        }

        // 0 = 0%, 1 = 100%
        public double Progress
        {
            get => _progress;
            set
            {
                _progress = value;
                NotifyPropertyChanged("Progress");
                NotifyPropertyChanged("ProgressBarVisibility");
            }
        }

        public int Count
        {
            get => _count;
            set
            {
                _count = value;
                NotifyPropertyChanged("Count");
                NotifyPropertyChanged("CountVisibility");
            }
        }

        public int MinCount
        {
            get => _minCount;
            set
            {
                _minCount = value;
                NotifyPropertyChanged("MinCount");
                NotifyPropertyChanged("CountVisibility");
            }
        }

        // Only make the progress bar visible if the item is partially watched.
        public Visibility ProgressBarVisibility
        {
            get
            {
                if (_progress > 0 && !_watched) { return Visibility.Visible; }
                else { return Visibility.Collapsed; }
            }
        }

        // Hide the count if it's less than the minimum allowed count value.
        public Visibility CountVisibility
        {
            get
            {
                return _count >= _minCount ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public Poster()
        {
            InitializeComponent();
        }
    }
}
